
<%@ page contentType="text/html" %>

<html>
  <head>
    <title>Shift Notification</title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css"/>
    <link rel="stylesheet" href="${resource(dir: 'css/bootstrap-dist/css', file: 'bootstrap.min.css')}" type="text/css"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'custom.styles.css')}" type="text/css"/>
    
  </head>
  <body class="body">
      <div class="container">
          <div class="center-block"><img src="${resource(dir: 'images', file: 'logo.jpg')}" alt="KK HealthCare"></div>
        <br>
        <br>
        <br/>
        You have a new shift!
        <fieldset>
            <legend>Shift Details</legend>
        <ol class="property-list shift">
			
				<g:if test="${shiftInstance?.shiftdate}">
				<li class="fieldcontain">
					<span id="shiftdate-label" class="property-label"><g:message code="shift.shiftdate.label" default="Shiftdate" /></span>
					
						<span class="property-value" aria-labelledby="shiftdate-label"><g:fieldValue bean="${shiftInstance}" field="shiftdate"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${shiftInstance?.shifttype}">
				<li class="fieldcontain">
					<span id="shifttype-label" class="property-label"><g:message code="shift.shifttype.label" default="Shifttype" /></span>
					
						<span class="property-value" aria-labelledby="shifttype-label"><g:fieldValue bean="${shiftInstance}" field="shifttype"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${shiftInstance?.home}">
				<li class="fieldcontain">
					<span id="home-label" class="property-label"><g:message code="shift.home.label" default="Home" /></span>
					
						<span class="property-value" aria-labelledby="home-label">${shiftInstance?.home?.encodeAsHTML()}</span>
					
				</li>
				</g:if>
			
			
				<g:if test="${shiftInstance?.status}">
				<li class="fieldcontain">
					<span id="status-label" class="property-label"><g:message code="shift.status.label" default="Status" /></span>
					
						<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${shiftInstance}" field="status"/></span>
					
				</li>
				</g:if>
                                
                                <g:if test="${shiftInstance?.shifthours}">
				<li class="fieldcontain">
					<span id="shifthours-label" class="property-label">Hours</span>
					
						<span class="property-value" aria-labelledby="shifthours-label"><g:fieldValue bean="${shiftInstance}" field="shifthours"/></span>
					
				</li>
				</g:if>
			
			</ol>
                        Contact Us for more details
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
        </fieldset>
      </div>
  </body>
</html>
