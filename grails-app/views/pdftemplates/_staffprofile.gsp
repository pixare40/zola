<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
  <head>
    <title>Staff Report</title>
        <style  type="text/css">
            @page {
            size: 8in 11.5in; /* width height */
            margin: 0.25in;
            }
            body{
            font-family:"Verdana",Times,sans-serif;
            font-size:12px;
            }
            table td{
            border:1px solid #000;
            }
          </style>
  </head>
  <body>
      <div class="container" border="1">
          <h1 style="text-align:center;">Agency Staff Confirmation</h1>
          <p>Agency Name: KK HealthCare Limited</p>
          <table style="width:700px;">
              <tbody>
                  <tr>
                      <td><strong>Candidate Name</strong></td>
                      <td>${staffInstance.candidatename}</td>
                      <td><strong>Known As</strong></td>
                      <td>${staffInstance.knownas}</td>
                  </tr>
                  <tr>
                      <td rowspan="2"><strong>Address</strong></td>
                      <td rowspan="2">${staffInstance.address}</td>
                      <td><strong>Contact Number</strong></td>
                      <td>${staffInstance.contactnumber}</td>
                  </tr>
                  <tr>
                      <td><strong>D.O.B</strong></td>
                      <td>${staffInstance.dateofbirth}</td>
                  </tr>
                  <tr>
                      <td><strong>Date of last DBS Check</strong></td>
                      <td>${staffInstance.dateoflastDBScheck}</td>
                      <td><strong>DBS No</strong></td>
                      <td>${staffInstance.dbsnumber}</td>
                  </tr>
                  <tr>
                      <td colspan="3"><strong>Job Role To be Undertaken</strong></td>
                      <td>${staffInstance.stafftype}</td>
                  </tr>
              </tbody>
          </table>
          <br/>
          <br/>
          <table style="width:700px;">
              <thead>
                  <tr>
                      <th><strong>Checks</strong></th>
                      <th><strong>Confirmed</strong></th>
                      <th><strong>Passport Picture</strong></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td><strong>Staff Eligible To Work in the UK?</strong></td>
                      <td><g:formatBoolean boolean="${staffInstance.eligibletoworkintheuk}" true="Yes" false="No" /></td>
                      <td rowspan="8"><rendering:inlinePng bytes="${avatar}" width="100" height="100" /></td>
                  </tr>
                  <tr>
                      <td><strong>Passport</strong></td>
                      <td><g:formatBoolean boolean="${staffInstance.passport}" true="Yes" false="No" /></td>
                      
                  </tr>
                  <tr>
                      <td><strong>Visa (Up to date)</strong></td>
                      <td>${staffInstance.visauptodate}</td>
                  </tr>
                  <tr>
                      <td><strong>Confirmation of identity</strong></td>
                      <td><g:formatBoolean boolean="${staffInstance.confirmationofidentity}" true="Yes" false="No" /></td>
                  </tr>
                  <tr>
                      <td><strong>Proof of Address</strong></td>
                      <td><g:formatBoolean boolean="${staffInstance.proofofaddress}" true="Yes" false="No" /></td>
                  </tr>
                  <tr>
                      <td><strong>References Received</strong></td>
                      <td><g:formatBoolean boolean="${staffInstance.referencesreceived}" true="Yes" false="No" /></td>
                  </tr>
                  <tr>
                      <td><strong>Driving License/Insurance</strong></td>
                      <td><g:formatBoolean boolean="${staffInstance.drivinglicenseandinsurance}" true="Yes" false="No" /></td>
                  </tr>
              </tbody>
          </table>
            <br/>
            <br/>
            <table style="width:700px;">
              <thead>
                  <tr>
                      <th><strong>Training</strong></th>
                      <th><strong>Undertaken (Yes/No)</strong></th>
                      <th><strong>Completed</strong></th>
                  </tr>
              </thead>
              <tbody>
                  <g:each in="${trainings}">
                        <tr>
                                <td>${it.training}</td>

                                <td><g:formatBoolean boolean="${it.undertaken}" true="Yes" false="No" /></td>

                                <td>${it.datecompleted}</td>

                        </tr>
                </g:each>
              </tbody>
            </table>
      </div>
  </body>
</html>
