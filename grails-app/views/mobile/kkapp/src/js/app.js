angular.module('KKApp', [
  'ngRoute',
  'mobile-angular-ui',
  'KKApp.controllers.Main'
])

.config(function($routeProvider) {
  $routeProvider.when('/', {templateUrl:'home.html',  reloadOnSearch: false});
});