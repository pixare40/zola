<%@ page import="com.zola.shyft.Client" %>



<div class="fieldcontain ${hasErrors(bean: clientInstance, field: 'clientname', 'error')} required">
	<label for="clientname">
		<g:message code="client.clientname.label" default="Clientname" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="clientname" required="" value="${clientInstance?.clientname}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: clientInstance, field: 'telephonenumber', 'error')} required">
	<label for="telephonenumber">
		<g:message code="client.telephonenumber.label" default="Telephonenumber" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="telephonenumber" required="" value="${clientInstance?.telephonenumber}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: clientInstance, field: 'contactemail', 'error')} required">
	<label for="contactemail">
		<g:message code="client.contactemail.label" default="Contactemail" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="contactemail" required="" value="${clientInstance?.contactemail}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: clientInstance, field: 'address', 'error')} required">
	<label for="address">
		<g:message code="client.address.label" default="Address" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="address" required="" value="${clientInstance?.address}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: clientInstance, field: 'owner', 'error')} ">
	<label for="owner">
		<g:message code="client.owner.label" default="Owner" />
		
	</label>
	<g:textField name="owner" value="${clientInstance?.owner}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: clientInstance, field: 'contactperson', 'error')} required">
	<label for="contactperson">
		<g:message code="client.contactperson.label" default="Contactperson" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="contactperson" required="" value="${clientInstance?.contactperson}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: clientInstance, field: 'county', 'error')} required">
	<label for="county">
		<g:message code="client.county.label" default="County" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="county" required="" value="${clientInstance?.county}"/>

</div>

