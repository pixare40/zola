
<%@ page import="com.zola.shyft.Client" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'client.label', default: 'Client')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
                <r:require modules="bootstrap"/>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'custom.styles.css')}" type="text/css">
	</head>
	<body>
		<g:render template="/shared/menu" />
		<div class="container">
                    <div class="col-xs-3">
                        <g:render template="clientactions"/>
                    </div>
                    <div class="col-xs-8">
		<div id="list-client" class="content scaffold-list" role="main">
                        <fieldset>
                            <legend>Client List</legend>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-hover">
			<thead>
					<tr>
					
						<g:sortableColumn property="clientname" title="Client" />
					
						<g:sortableColumn property="telephonenumber" title="Phone Number" />
					
						<g:sortableColumn property="contactemail" title="Email" />
					
						<g:sortableColumn property="address" title="Address" />
					
						<g:sortableColumn property="owner" title="Owner" />
					
						<g:sortableColumn property="contactperson" title="Contact Person" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${clientInstanceList}" status="i" var="clientInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${clientInstance.id}">${fieldValue(bean: clientInstance, field: "clientname")}</g:link></td>
					
						<td>${fieldValue(bean: clientInstance, field: "telephonenumber")}</td>
					
						<td>${fieldValue(bean: clientInstance, field: "contactemail")}</td>
					
						<td>${fieldValue(bean: clientInstance, field: "address")}</td>
					
						<td>${fieldValue(bean: clientInstance, field: "owner")}</td>
					
						<td>${fieldValue(bean: clientInstance, field: "contactperson")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			
				<g:paginate total="${clientInstanceCount ?: 0}" />
                        </fieldset>
                </div>
		</div>
                </div>
	</body>
</html>
