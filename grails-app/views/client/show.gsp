
<%@ page import="com.zola.shyft.Client" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'client.label', default: 'Client')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
                <r:require modules="bootstrap"/>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'custom.styles.css')}" type="text/css">
	</head>
	<body>
            <g:render template="/shared/menu" />
		<a href="#show-client" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="container">
                    <div class="col-xs-3">
                        <g:render template="clientactions"/>
                        
                    </div>
            <div class="col-xs-8">
		<div id="show-client" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list client">
			
				<g:if test="${clientInstance?.clientname}">
				<li class="fieldcontain">
					<span id="clientname-label" class="property-label"><g:message code="client.clientname.label" default="Clientname" /></span>
					
						<span class="property-value" aria-labelledby="clientname-label"><g:fieldValue bean="${clientInstance}" field="clientname"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${clientInstance?.telephonenumber}">
				<li class="fieldcontain">
					<span id="telephonenumber-label" class="property-label"><g:message code="client.telephonenumber.label" default="Telephonenumber" /></span>
					
						<span class="property-value" aria-labelledby="telephonenumber-label"><g:fieldValue bean="${clientInstance}" field="telephonenumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${clientInstance?.contactemail}">
				<li class="fieldcontain">
					<span id="contactemail-label" class="property-label"><g:message code="client.contactemail.label" default="Contactemail" /></span>
					
						<span class="property-value" aria-labelledby="contactemail-label"><g:fieldValue bean="${clientInstance}" field="contactemail"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${clientInstance?.address}">
				<li class="fieldcontain">
					<span id="address-label" class="property-label"><g:message code="client.address.label" default="Address" /></span>
					
						<span class="property-value" aria-labelledby="address-label"><g:fieldValue bean="${clientInstance}" field="address"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${clientInstance?.owner}">
				<li class="fieldcontain">
					<span id="owner-label" class="property-label"><g:message code="client.owner.label" default="Owner" /></span>
					
						<span class="property-value" aria-labelledby="owner-label"><g:fieldValue bean="${clientInstance}" field="owner"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${clientInstance?.contactperson}">
				<li class="fieldcontain">
					<span id="contactperson-label" class="property-label"><g:message code="client.contactperson.label" default="Contactperson" /></span>
					
						<span class="property-value" aria-labelledby="contactperson-label"><g:fieldValue bean="${clientInstance}" field="contactperson"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${clientInstance?.county}">
				<li class="fieldcontain">
					<span id="county-label" class="property-label"><g:message code="client.county.label" default="County" /></span>
					
						<span class="property-value" aria-labelledby="county-label"><g:fieldValue bean="${clientInstance}" field="county"/></span>
					
				</li>
				</g:if>
			
			</ol>
                        <g:link class="btn btn-primary" style="color:white;font-size:12px;" controller="shift" action="pastshifts" id="${clientInstance.id}" >View Past Shifts</g:link>
                        <g:link class="btn btn-primary" style="color:white;font-size:12px;" controller="staff" action="paststaff" id="${clientInstance.id}" >View Past Staff to Work Here</g:link>
			<g:form url="[resource:clientInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${clientInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
                </div>
                </div>
	</body>
</html>
