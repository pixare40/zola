<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
                <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
                <link href='http://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>
                <link href='http://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
                <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
                <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
                <style>
                    .body {
                      font-family: 'Raleway', sans-serif;
                      font-size: 13px;
                    }
                    .admin-title{
                      font-family: 'Montserrat', sans-serif;
                      font-size: 16px;
                    }
                    .admin-details{
                        font-family: 'Oxygen', sans-serif;
                        font-size:14px;
                    }
                    .btn a{
                        color:white;
                    }
                    .btn a:hover{
                        text-decoration:none;
                    }
                    
                </style>
		<g:layoutHead/>
                
		<g:javascript library="application"/>
                <g:javascript library="jquery" plugin="jquery"/>
                <jqui:resources components="datepicker" mode="normal" />
                <g:javascript src="modernizr.js"></g:javascript>
		<r:layoutResources />
                <style>
                    legend{
                        border-width: 0px 0px 2px;
                        border-style: none none solid;
                        border-color: -moz-use-text-color -moz-use-text-color #2E96DB;
                    }
                </style>
	</head>
	<body class="body">
		<div id="zolalogo" role="banner"><g:link controller="home" action="index" style="font-family: 'Montserrat', sans-serif; font-size: 40px; text-decoration: none; color: #2e96db">KK Staffing System</g:link></div>
		<g:layoutBody/>
		<div class="footer" role="contentinfo" style="color:white;">
                    <g:set var="months" value="${new java.text.DateFormatSymbols().months}"/>
                    <g:set var="today" value="${new Date()}"/>
                    &copy; Copyright ${today[Calendar.YEAR]}, Software developed by <a href="http://uk.linkedin.com/pub/kabaji-egara/8a/384/a40/" target="_blank">Kabaji Egara</a>
                    <div style="float: right">Powered by Grails</div>
                </div>
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
		<r:layoutResources />
	</body>
</html>
