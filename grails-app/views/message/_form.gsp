


<div class="fieldcontain ${hasErrors(bean: messageInstance, field: 'recipient', 'error')} required">
	<label for="recipient">
		<g:message code="message.recipient.label" default="Recipient" />
		<span class="required-indicator">*</span>
	</label>
	<g:select class="form-control" id="recipient" name="recipient.id" from="${com.zola.shyft.Staff.list()}" optionKey="id" required="" value="${messageInstance?.recipient?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: messageInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="message.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="title" required="" value="${messageInstance?.title}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: messageInstance, field: 'message', 'error')} required">
	<label for="message">
		<g:message code="message.message.label" default="Message" />
		<span class="required-indicator">*</span>
	</label>
                <textarea class="form-control" rows="5" name="message" required="" value="${messageInstance?.message}"></textarea>
</div>

