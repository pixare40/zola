
<%@ page import="com.zola.shyft.Message" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'message.label', default: 'Message')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
                <r:require modules="bootstrap"/>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'custom.styles.css')}" type="text/css">

	</head>
	<body>
		<a href="#list-message" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<g:render template="/shared/menu" />
                <div class="container">
                <div class="col-xs-3">
                    <g:render template="/message/messagingmenu"/>
                </div>
		<div id="list-message" class="content scaffold-list col-xs-9" role="main">
                    
                <fieldset>
                <legend>Sent Messages</legend>
                <g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table">
			<thead>
					<tr>
					
						<th><g:message code="message.recipient.label" default="Recipient" /></th>
					
						<g:sortableColumn property="title" title="${message(code: 'message.title.label', default: 'Title')}" />
										
						<th>Message Status</th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${messageInstanceList}" status="i" var="messageInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link controller="message" action="show" id="${messageInstance.id}">${fieldValue(bean: messageInstance, field: "recipient")}</g:link></td>
										
						<td>${fieldValue(bean: messageInstance, field: "title")}</td>
										
						<td><g:if test="${messageInstance.readproperty == true}">Read</g:if>
                                                    <g:else>Unread</g:else>
                                                    </td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${messageInstanceCount ?: 0}" />
			</div>
                </fieldset>
		</div>
                </div>
	</body>
</html>
