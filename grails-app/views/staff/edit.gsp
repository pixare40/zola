<%@ page import="com.zola.shyft.Staff" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'staff.label', default: 'Staff')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
                <r:require modules="bootstrap"/>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'custom.styles.css')}" type="text/css">
	</head>
	<body>
            <g:render template="/shared/menu" />
		<a href="#edit-staff" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="container">
                   <div class="col-xs-3">
                        <g:render template="staffactions"/>
                    </div>
            <div class="col-xs-8">
		<div id="edit-staff" class="content scaffold-edit" role="main">
			<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${staffInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${staffInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
                        <g:uploadForm action='uploadprofilepicture'>
                            <g:hiddenField name="id" value="${staffInstance?.id}"/>
                            <input type="file" name="avatar" id="avatar" />
                            <input type="submit" class="buttons" value="Upload" />
                         </g:uploadForm>
			<g:form url="[resource:staffInstance, action:'update']" method="PUT" >
				<g:hiddenField name="version" value="${staffInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</fieldset>
			</g:form>
		</div>
                </div>
            </div>
	</body>
</html>
