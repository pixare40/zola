<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
   <meta name="layout" content="main" />
   <title>Upload</title>
</head>

<body>
   <div class="body">

      <g:uploadForm action='upload'>
         <input type="file" name="avatar" id="avatar" />
         <input type="submit" class="buttons" value="Upload" />
      </g:uploadForm>

   </div>
</body>

</html>