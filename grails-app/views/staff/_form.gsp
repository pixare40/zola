<%@ page import="com.zola.shyft.Staff" %>


<fieldset>
    <legend>Authentication and Authorization Information</legend>
<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="staff.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${staffInstance?.username}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="staff.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="password" required="" value="${staffInstance?.password}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="staff.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="email" required="" value="${staffInstance?.email}"/>

</div>
</fieldset>
        <fieldset>
            <legend>Staff Information</legend>
<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'stafftype', 'error')} required">
	<label for="stafftype">
		Staff Type
		<span class="required-indicator">*</span>
	</label>
	<g:select name="stafftype" required="" from="${['Nurse','Support Worker','Carer','Home Care','Live-in Care']}" value="${staffInstance?.stafftype}" valueMessagePrefix="staff.stafftype"/>

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'candidatename', 'error')} required">
	<label for="candidatename">
		<g:message code="staff.candidatename.label" default="Name of Employee" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="candidatename" required="" value="${staffInstance?.candidatename}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'knownas', 'error')} ">
	<label for="knownas">
		<g:message code="staff.knownas.label" default="Known as Name" />
		
	</label>
	<g:textField name="knownas" value="${staffInstance?.knownas}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'address', 'error')} required">
	<label for="address">
		<g:message code="staff.address.label" default="Address" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="address" required="" value="${staffInstance?.address}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'contactnumber', 'error')} required">
	<label for="contactnumber">
		<g:message code="staff.contactnumber.label" default="Contact Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="contactnumber" required="" value="${staffInstance?.contactnumber}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'dateofbirth', 'error')} required">
	<label for="dateofbirth">
		<g:message code="staff.dateofbirth.label" default="Date of Birth" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="dateofbirth" precision="day"  value="${staffInstance?.dateofbirth}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'dateoflastDBScheck', 'error')} required">
	<label for="dateoflastDBScheck">
		<g:message code="staff.dateoflastDBScheck.label" default="Date of last DBS check" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="dateoflastDBScheck" precision="day"  value="${staffInstance?.dateoflastDBScheck}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'dbsnumber', 'error')} required">
	<label for="dbsnumber">
		<g:message code="staff.dbsnumber.label" default="DBS Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="dbsnumber" required="" value="${staffInstance?.dbsnumber}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'eligibletoworkintheuk', 'error')} ">
	<label for="eligibletoworkintheuk">
		<g:message code="staff.eligibletoworkintheuk.label" default="Eligible To Work in The UK?" />
		
	</label>
	<g:checkBox name="eligibletoworkintheuk" value="${staffInstance?.eligibletoworkintheuk}" />

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'passport', 'error')} ">
	<label for="passport">
		<g:message code="staff.passport.label" default="Passport" />
		
	</label>
	<g:checkBox name="passport" value="${staffInstance?.passport}" />

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'visauptodate', 'error')} required">
	<label for="visauptodate">
		<g:message code="staff.visauptodate.label" default="Visa Up-to-Date?" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="visauptodate" required="" value="${staffInstance?.visauptodate}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'confirmationofidentity', 'error')} ">
	<label for="confirmationofidentity">
		<g:message code="staff.confirmationofidentity.label" default="Confirmation of Identity" />
		
	</label>
	<g:checkBox name="confirmationofidentity" value="${staffInstance?.confirmationofidentity}" />

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'proofofaddress', 'error')} ">
	<label for="proofofaddress">
		<g:message code="staff.proofofaddress.label" default="Proof of Address" />
		
	</label>
	<g:checkBox name="proofofaddress" value="${staffInstance?.proofofaddress}" />

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'referencesreceived', 'error')} ">
	<label for="referencesreceived">
		<g:message code="staff.referencesreceived.label" default="References Received" />
		
	</label>
	<g:checkBox name="referencesreceived" value="${staffInstance?.referencesreceived}" />

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'drivinglicenseandinsurance', 'error')} ">
	<label for="drivinglicenseandinsurance">
		<g:message code="staff.drivinglicenseandinsurance.label" default="Driving License and Insurance" />
		
	</label>
	<g:checkBox name="drivinglicenseandinsurance" value="${staffInstance?.drivinglicenseandinsurance}" />

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'allrequiredpaperwork', 'error')} ">
	<label for="allrequiredpaperwork">
		<g:message code="staff.allrequiredpaperwork.label" default="All Required Paperwork" />
		
	</label>
	<g:checkBox name="allrequiredpaperwork" value="${staffInstance?.allrequiredpaperwork}" />

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'mandatorytraininguptodate', 'error')} ">
	<label for="mandatorytraininguptodate">
		<g:message code="staff.mandatorytraininguptodate.label" default="Mandatory Training Up-to-Date?" />
		
	</label>
	<g:checkBox name="mandatorytraininguptodate" value="${staffInstance?.mandatorytraininguptodate}" />

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'registeredunderdisabilityact', 'error')} ">
	<label for="registeredunderdisabilityact">
		<g:message code="staff.registeredunderdisabilityact.label" default="Registered Under The Disability Act?" />
		
	</label>
	<g:checkBox name="registeredunderdisabilityact" value="${staffInstance?.registeredunderdisabilityact}" />

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'staffhappy', 'error')} ">
	<label for="staffhappy">
		<g:message code="staff.staffhappy.label" default="Is the staff happy?" />
		
	</label>
	<g:checkBox name="staffhappy" value="${staffInstance?.staffhappy}" />

</div>
   
<g:if test="${staffInstance}">
<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'trainings', 'error')} ">
	<label for="trainings">
		<g:message code="staff.trainings.label" default="Trainings" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${staffInstance?.trainings?}" var="t">
    <li><g:link controller="mandatoryTraining" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="mandatoryTraining" action="create" params="['staff.id': staffInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'mandatoryTraining.label', default: 'MandatoryTraining')])}</g:link>
</li>
</ul>


</div>
</g:if>
<fieldset>
    <legend>Pay Package Information</legend>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Pay Type</th>
            <th>Weekday Rate</th>
            <th>Weekend Rate</th>
            <th>Bank Holiday Rate</th>
        </th>
        </thead>
        <tbody>
            <tr>
                <td>Early</td>
                <td><g:textField  name="earlyrateWD" placeholder="Early Rate WD" value="${staffInstance?.paypackage?.earlyrateWD}" /></td>
                <td><g:textField placeholder="Early Rate WE" name="earlyrateWE" value="${staffInstance?.paypackage?.earlyrateWE}" /></td>
                <td><g:textField  placeholder="Early Rate BH" name="earlyrateBH" value="${staffInstance?.paypackage?.earlyrateBH}" /></td>
            </tr>
            <tr>
                <td>Sleep</td>
                <td><g:textField  placeholder="Sleep Rate WD" name="sleeprateWD" value="${staffInstance?.paypackage?.sleeprateWD}" /></td>
                <td><g:textField  placeholder="Sleep Rate WE" name="sleeprateWE" value="${staffInstance?.paypackage?.sleeprateWE}" /></td>
                <td><g:textField placeholder="Sleep Rate BH" name="sleeprateBH" value="${staffInstance?.paypackage?.sleeprateBH}" /></td>
            </tr>
            <tr>
                <td>Late</td>
                <td><g:textField  placeholder="Late Rate WD" name="laterateWD" value="${staffInstance?.paypackage?.laterateWD}" /></td>
                <td><g:textField  placeholder="Late Rate WE" name="laterateWE" value="${staffInstance?.paypackage?.laterateWE}" /></td>
                <td><g:textField  placeholder="Late Rate BH" name="laterateBH" value="${staffInstance?.paypackage?.laterateBH}" /></td>
            </tr>
            <tr>
                <td>Wake Night</td>
                <td><g:textField  placeholder="Wake Night Rate WD" name="wakenightrateWD" value="${staffInstance?.paypackage?.wakenightrateWD}" /></td>
                <td><g:textField  placeholder="Wake Night Rate WE" name="wakenightrateWE" value="${staffInstance?.paypackage?.wakenightrateWE}" /></td>
                <td><g:textField  placeholder="Wake Night Rate BH" name="wakenightrateBH" value="${staffInstance?.paypackage?.wakenightrateBH}" /></td>
            </tr>
            <tr>
                <td>Day Care</td>
                <td><g:textField  placeholder="Day care WD" name="daycarerateWD" value="${staffInstance?.paypackage?.daycarerateWD}" /></td>
                <td><g:textField  placeholder="Day Care WE" name="daycarerateWE" value="${staffInstance?.paypackage?.daycarerateWE}" /></td>
                <td><g:textField  placeholder="Day Care BH" name="daycarerateBH" value="${staffInstance?.paypackage?.daycarerateBH}" /></td>
            </tr>
            <tr>
                <td>Long Day</td>
                <td><g:textField  placeholder="Long Day WD" name="longdayrateWD" value="${staffInstance?.paypackage?.longdayrateWD}" /></td>
                <td><g:textField  placeholder="Long Day WE" name="longdayrateWE" value="${staffInstance?.paypackage?.longdayrateWE}" /></td>
                <td><g:textField  placeholder="Long Day BH" name="longdayrateBH" value="${staffInstance?.paypackage?.longdayrateBH}" /></td>

            </tr>
        </tbody>
    </table>
</fieldset>

</fieldset>
    <h3>Warning, Dragons Ahead, Administrator functions, These values impact how the staff is able to login on their account and view their data</h3>
<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'accountExpired', 'error')} ">
	<label for="accountExpired">
		<g:message code="staff.accountExpired.label" default="Account Expired" />
		
	</label>
	<g:checkBox name="accountExpired" value="${staffInstance?.accountExpired}" />
</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'accountLocked', 'error')} ">
	<label for="accountLocked">
		<g:message code="staff.accountLocked.label" default="Account Locked" />
		
	</label>
	<g:checkBox name="accountLocked" value="${staffInstance?.accountLocked}" />
</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'enabled', 'error')} ">
	<label for="enabled">
		<g:message code="staff.enabled.label" default="Enabled" />
		
	</label>
	<g:checkBox name="enabled" value="${staffInstance?.enabled}" />

</div>

<div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'passwordExpired', 'error')} ">
	<label for="passwordExpired">
		<g:message code="staff.passwordExpired.label" default="Password Expired" />
		
	</label>
	<g:checkBox name="passwordExpired" value="${staffInstance?.passwordExpired}" />

</div>

