-
<%@ page import="com.zola.shyft.Staff" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'staff.label', default: 'Staff')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
                <r:require modules="bootstrap"/>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'custom.styles.css')}" type="text/css">
	</head>
	<body>
            <g:render template="/shared/menu" />
		<div class="container">
                    <div class="col-xs-3">
                        <g:render template="staffactions"/>
                    </div>
            <div class="col-xs-8">
                        <fieldset>
                            <legend>Staff List</legend>
		<div id="list-staff" class="content scaffold-list" role="main">
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-hover">
			<thead>
					<tr>
                                            
					
						<g:sortableColumn property="email" title="Email" />
					
						<g:sortableColumn property="candidatename" title="Name" />
					
						<g:sortableColumn property="knownas" title="Known As" />
					
						<g:sortableColumn property="address" title="Address" />
                                                
                                                <th> Profile Picture</th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${staffInstanceList}" status="i" var="staffInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${staffInstance.id}">${fieldValue(bean: staffInstance, field: "email")}</g:link></td>
                                                
						<td>${fieldValue(bean: staffInstance, field: "candidatename")}</td>
					
						<td>${fieldValue(bean: staffInstance, field: "knownas")}</td>
					
						<td>${fieldValue(bean: staffInstance, field: "address")}</td>
                                                
                                                <g:if test="${staffInstance.avatar != null}">
                                                <td><img src="${resource(dir: 'avatarImage', file: staffInstance.id+".png")}" width='50' height="50"/></td>
                                                </g:if>
                                                <g:else>
                                                <td><img src="${resource(dir: 'avatarImage', file: "noprofile.jpg")}" width='50' height="50"/></td>
                                                </g:else>
					
					</tr>
				</g:each>
				</tbody>
			</table>
                        <g:if test="${staffInstanceCount > 10}">
			<ul class="pagination">
				<g:paginate total="${staffInstanceCount ?: 0}" />
			</ul>
                        </g:if>
		</div>
                        </fieldset>
            </div>
                </div>
	</body>
</html>
