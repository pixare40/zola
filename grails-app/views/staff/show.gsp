
<%@ page import="com.zola.shyft.Staff" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'staff.label', default: 'Staff')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
                <r:require modules="bootstrap"/>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'custom.styles.css')}" type="text/css">
	</head>
	<body>
            <g:render template="/shared/menu" />
		<a href="#show-staff" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="container">
                    <div class="col-xs-3">
                        <g:render template="staffactions"/>
                    </div>
            <div class="col-xs-8">
                <fieldset>
                    <legend>Staff Information for ${staffInstance.candidatename}</legend>
                    <span><g:link action="exportStaffToPDF" id="${staffInstance.id}">Export to PDF</g:link></span>
		<div id="show-staff" class="content scaffold-show" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
                        
                        <g:if test="${staffInstance.avatar}">
                            <div>
                            <img src="${resource(dir: 'avatarImage', file: staffInstance.id+".png")}" width='300' height="300"/>
                            </div>
                            </g:if>
                        <g:else>
                        <div class="fieldcontain ${hasErrors(bean: staffInstance, field: 'avatar', 'error')} required">
                                <label for="avatar">
                                        Upload Profile Picture
                                </label>
                                <g:uploadForm action='uploadprofilepicture'>
                                    <g:hiddenField name="id" value="${staffInstance?.id}"/>
                                    <input type="file" name="avatar" id="avatar" />
                                    <input type="submit" class="buttons" value="Upload" />
                                 </g:uploadForm>
                        </div>
                        </g:else>
			<ol class="property-list staff">
                            
                            
			
				<g:if test="${staffInstance?.username}">
				<li class="fieldcontain">
					<span id="username-label" class="property-label"><g:message code="staff.username.label" default="Username" /></span>
					
						<span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${staffInstance}" field="username"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.email}">
				<li class="fieldcontain">
					<span id="email-label" class="property-label"><g:message code="staff.email.label" default="Email" /></span>
					
						<span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${staffInstance}" field="email"/></span>
					
				</li>
				</g:if>
                                
                                <g:if test="${staffInstance?.stafftype}">
				<li class="fieldcontain">
					<span id="stafftype-label" class="property-label">Type Of Staff</span>
					
						<span class="property-value" aria-labelledby="stafftype-label"><g:fieldValue bean="${staffInstance}" field="stafftype"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.candidatename}">
				<li class="fieldcontain">
					<span id="candidatename-label" class="property-label"><g:message code="staff.candidatename.label" default="Candidatename" /></span>
					
						<span class="property-value" aria-labelledby="candidatename-label"><g:fieldValue bean="${staffInstance}" field="candidatename"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.knownas}">
				<li class="fieldcontain">
					<span id="knownas-label" class="property-label"><g:message code="staff.knownas.label" default="Knownas" /></span>
					
						<span class="property-value" aria-labelledby="knownas-label"><g:fieldValue bean="${staffInstance}" field="knownas"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.address}">
				<li class="fieldcontain">
					<span id="address-label" class="property-label"><g:message code="staff.address.label" default="Address" /></span>
					
						<span class="property-value" aria-labelledby="address-label"><g:fieldValue bean="${staffInstance}" field="address"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.contactnumber}">
				<li class="fieldcontain">
					<span id="contactnumber-label" class="property-label"><g:message code="staff.contactnumber.label" default="Contactnumber" /></span>
					
						<span class="property-value" aria-labelledby="contactnumber-label"><g:fieldValue bean="${staffInstance}" field="contactnumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.dateofbirth}">
				<li class="fieldcontain">
					<span id="dateofbirth-label" class="property-label"><g:message code="staff.dateofbirth.label" default="Dateofbirth" /></span>
					
						<span class="property-value" aria-labelledby="dateofbirth-label"><g:formatDate date="${staffInstance?.dateofbirth}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.dateoflastDBScheck}">
				<li class="fieldcontain">
					<span id="dateoflastDBScheck-label" class="property-label"><g:message code="staff.dateoflastDBScheck.label" default="Dateoflast DBS check" /></span>
					
						<span class="property-value" aria-labelledby="dateoflastDBScheck-label"><g:formatDate date="${staffInstance?.dateoflastDBScheck}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.dbsnumber}">
				<li class="fieldcontain">
					<span id="dbsnumber-label" class="property-label"><g:message code="staff.dbsnumber.label" default="Dbsnumber" /></span>
					
						<span class="property-value" aria-labelledby="dbsnumber-label"><g:fieldValue bean="${staffInstance}" field="dbsnumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.eligibletoworkintheuk}">
				<li class="fieldcontain">
					<span id="eligibletoworkintheuk-label" class="property-label"><g:message code="staff.eligibletoworkintheuk.label" default="Eligibletoworkintheuk" /></span>
					
						<span class="property-value" aria-labelledby="eligibletoworkintheuk-label"><g:formatBoolean boolean="${staffInstance?.eligibletoworkintheuk}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.passport}">
				<li class="fieldcontain">
					<span id="passport-label" class="property-label"><g:message code="staff.passport.label" default="Passport" /></span>
					
						<span class="property-value" aria-labelledby="passport-label"><g:formatBoolean boolean="${staffInstance?.passport}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.visauptodate}">
				<li class="fieldcontain">
					<span id="visauptodate-label" class="property-label"><g:message code="staff.visauptodate.label" default="Visauptodate" /></span>
					
						<span class="property-value" aria-labelledby="visauptodate-label"><g:fieldValue bean="${staffInstance}" field="visauptodate"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.confirmationofidentity}">
				<li class="fieldcontain">
					<span id="confirmationofidentity-label" class="property-label"><g:message code="staff.confirmationofidentity.label" default="Confirmationofidentity" /></span>
					
						<span class="property-value" aria-labelledby="confirmationofidentity-label"><g:formatBoolean boolean="${staffInstance?.confirmationofidentity}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.proofofaddress}">
				<li class="fieldcontain">
					<span id="proofofaddress-label" class="property-label"><g:message code="staff.proofofaddress.label" default="Proofofaddress" /></span>
					
						<span class="property-value" aria-labelledby="proofofaddress-label"><g:formatBoolean boolean="${staffInstance?.proofofaddress}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.referencesreceived}">
				<li class="fieldcontain">
					<span id="referencesreceived-label" class="property-label"><g:message code="staff.referencesreceived.label" default="Referencesreceived" /></span>
					
						<span class="property-value" aria-labelledby="referencesreceived-label"><g:formatBoolean boolean="${staffInstance?.referencesreceived}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.drivinglicenseandinsurance}">
				<li class="fieldcontain">
					<span id="drivinglicenseandinsurance-label" class="property-label"><g:message code="staff.drivinglicenseandinsurance.label" default="Drivinglicenseandinsurance" /></span>
					
						<span class="property-value" aria-labelledby="drivinglicenseandinsurance-label"><g:formatBoolean boolean="${staffInstance?.drivinglicenseandinsurance}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.allrequiredpaperwork}">
				<li class="fieldcontain">
					<span id="allrequiredpaperwork-label" class="property-label"><g:message code="staff.allrequiredpaperwork.label" default="Allrequiredpaperwork" /></span>
					
						<span class="property-value" aria-labelledby="allrequiredpaperwork-label"><g:formatBoolean boolean="${staffInstance?.allrequiredpaperwork}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.mandatorytraininguptodate}">
				<li class="fieldcontain">
					<span id="mandatorytraininguptodate-label" class="property-label"><g:message code="staff.mandatorytraininguptodate.label" default="Mandatorytraininguptodate" /></span>
					
						<span class="property-value" aria-labelledby="mandatorytraininguptodate-label"><g:formatBoolean boolean="${staffInstance?.mandatorytraininguptodate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.registeredunderdisabilityact}">
				<li class="fieldcontain">
					<span id="registeredunderdisabilityact-label" class="property-label"><g:message code="staff.registeredunderdisabilityact.label" default="Registeredunderdisabilityact" /></span>
					
						<span class="property-value" aria-labelledby="registeredunderdisabilityact-label"><g:formatBoolean boolean="${staffInstance?.registeredunderdisabilityact}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${staffInstance?.staffhappy}">
				<li class="fieldcontain">
					<span id="staffhappy-label" class="property-label"><g:message code="staff.staffhappy.label" default="Staffhappy" /></span>
					
						<span class="property-value" aria-labelledby="staffhappy-label"><g:formatBoolean boolean="${staffInstance?.staffhappy}" /></span>
					
				</li>
				</g:if>

				<g:if test="${staffInstance?.trainings}">
				<li class="fieldcontain">
					<span id="trainings-label" class="property-label"><g:message code="staff.trainings.label" default="Trainings" /></span>
					
						<g:each in="${staffInstance.trainings}" var="t">
						<span class="property-value" aria-labelledby="trainings-label"><g:link controller="mandatoryTraining" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
                        <fieldset>
                            <legend>Pay Package Information</legend>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Pay Type</th>
                                    <th>Weekday Rate</th>
                                    <th>Weekend Rate</th>
                                    <th>Bank Holiday Rate</th>
                                </th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Early</td>
                                        <td>${staffInstance?.paypackage?.earlyrateWD}</td>
                                        <td>${staffInstance?.paypackage?.earlyrateWE}</td>
                                        <td>${staffInstance?.paypackage?.earlyrateBH}</td>
                                    </tr>
                                    <tr>
                                        <td>Sleep</td>
                                        <td>${staffInstance?.paypackage?.sleeprateWD}</td>
                                        <td>${staffInstance?.paypackage?.sleeprateWE}</td>
                                        <td>${staffInstance?.paypackage?.sleeprateBH}</td>
                                    </tr>
                                    <tr>
                                        <td>Late</td>
                                        <td>${staffInstance?.paypackage?.laterateWD}</td>
                                        <td>${staffInstance?.paypackage?.laterateWE}</td>
                                        <td>${staffInstance?.paypackage?.laterateBH}</td>
                                    </tr>
                                    <tr>
                                        <td>Wake Night</td>
                                        <td>${staffInstance?.paypackage?.wakenightrateWD}</td>
                                        <td>${staffInstance?.paypackage?.wakenightrateWE}</td>
                                        <td>${staffInstance?.paypackage?.wakenightrateBH}</td>
                                    </tr>
                                    <tr>
                                        <td>Day Care</td>
                                        <td>${staffInstance?.paypackage?.daycarerateWD}</td>
                                        <td>${staffInstance?.paypackage?.daycarerateWE}</td>
                                        <td>${staffInstance?.paypackage?.daycarerateBH}</td>
                                    </tr>
                                    <tr>
                                        <td>Long Day</td>
                                        <td>${staffInstance?.paypackage?.longdayrateWD}</td>
                                        <td>${staffInstance?.paypackage?.longdayrateWE}</td>
                                        <td>${staffInstance?.paypackage?.longdayrateBH}</td>

                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
			<g:form url="[resource:staffInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${staffInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
                </fieldset>
                </div>
                </div>
	</body>
</html>
