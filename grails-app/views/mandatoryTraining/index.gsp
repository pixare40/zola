
<%@ page import="com.zola.shyft.MandatoryTraining" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'mandatoryTraining.label', default: 'MandatoryTraining')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-mandatoryTraining" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-mandatoryTraining" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="training" title="${message(code: 'mandatoryTraining.training.label', default: 'Training')}" />
					
						<g:sortableColumn property="undertaken" title="${message(code: 'mandatoryTraining.undertaken.label', default: 'Undertaken')}" />
					
						<g:sortableColumn property="datecompleted" title="${message(code: 'mandatoryTraining.datecompleted.label', default: 'Datecompleted')}" />
					
						<g:sortableColumn property="expirydate" title="${message(code: 'mandatoryTraining.expirydate.label', default: 'Expirydate')}" />
					
						<th><g:message code="mandatoryTraining.staff.label" default="Staff" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${mandatoryTrainingInstanceList}" status="i" var="mandatoryTrainingInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${mandatoryTrainingInstance.id}">${fieldValue(bean: mandatoryTrainingInstance, field: "training")}</g:link></td>
					
						<td><g:formatBoolean boolean="${mandatoryTrainingInstance.undertaken}" /></td>
					
						<td>${fieldValue(bean: mandatoryTrainingInstance, field: "datecompleted")}</td>
					
						<td>${fieldValue(bean: mandatoryTrainingInstance, field: "expirydate")}</td>
					
						<td>${fieldValue(bean: mandatoryTrainingInstance, field: "staff")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${mandatoryTrainingInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
