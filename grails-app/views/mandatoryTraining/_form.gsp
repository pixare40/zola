<%@ page import="com.zola.shyft.MandatoryTraining" %>



<div class="fieldcontain ${hasErrors(bean: mandatoryTrainingInstance, field: 'training', 'error')} required">
	<label for="training">
		<g:message code="mandatoryTraining.training.label" default="Training" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="training" required="" value="${mandatoryTrainingInstance?.training}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: mandatoryTrainingInstance, field: 'undertaken', 'error')} ">
	<label for="undertaken">
		<g:message code="mandatoryTraining.undertaken.label" default="Undertaken" />
		
	</label>
	<g:checkBox name="undertaken" value="${mandatoryTrainingInstance?.undertaken}" />

</div>

<div class="fieldcontain ${hasErrors(bean: mandatoryTrainingInstance, field: 'datecompleted', 'error')} ">
	<label for="datecompleted">
		<g:message code="mandatoryTraining.datecompleted.label" default="Datecompleted" />
		
	</label>
	<joda:dateField name="datecompleted" value="${mandatoryTrainingInstance?.datecompleted}" />

</div>

<div class="fieldcontain ${hasErrors(bean: mandatoryTrainingInstance, field: 'expirydate', 'error')} ">
	<label for="expirydate">
		<g:message code="mandatoryTraining.expirydate.label" default="Expirydate" />
		
	</label>
	<joda:dateField name="expirydate" value="${mandatoryTrainingInstance?.expirydate}" />

</div>

<div class="fieldcontain ${hasErrors(bean: mandatoryTrainingInstance, field: 'staff', 'error')} required">
	<label for="staff">
		<g:message code="mandatoryTraining.staff.label" default="Staff" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="staff" name="staff.id" from="${com.zola.shyft.Staff.list()}" optionKey="id" required="" value="${mandatoryTrainingInstance?.staff?.id}" class="many-to-one"/>

</div>

