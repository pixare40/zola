
<%@ page import="com.zola.shyft.MandatoryTraining" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'mandatoryTraining.label', default: 'MandatoryTraining')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-mandatoryTraining" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-mandatoryTraining" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list mandatoryTraining">
			
				<g:if test="${mandatoryTrainingInstance?.training}">
				<li class="fieldcontain">
					<span id="training-label" class="property-label"><g:message code="mandatoryTraining.training.label" default="Training" /></span>
					
						<span class="property-value" aria-labelledby="training-label"><g:fieldValue bean="${mandatoryTrainingInstance}" field="training"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mandatoryTrainingInstance?.undertaken}">
				<li class="fieldcontain">
					<span id="undertaken-label" class="property-label"><g:message code="mandatoryTraining.undertaken.label" default="Undertaken" /></span>
					
						<span class="property-value" aria-labelledby="undertaken-label"><g:formatBoolean boolean="${mandatoryTrainingInstance?.undertaken}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${mandatoryTrainingInstance?.datecompleted}">
				<li class="fieldcontain">
					<span id="datecompleted-label" class="property-label"><g:message code="mandatoryTraining.datecompleted.label" default="Datecompleted" /></span>
					
						<span class="property-value" aria-labelledby="datecompleted-label"><g:fieldValue bean="${mandatoryTrainingInstance}" field="datecompleted"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mandatoryTrainingInstance?.expirydate}">
				<li class="fieldcontain">
					<span id="expirydate-label" class="property-label"><g:message code="mandatoryTraining.expirydate.label" default="Expirydate" /></span>
					
						<span class="property-value" aria-labelledby="expirydate-label"><g:fieldValue bean="${mandatoryTrainingInstance}" field="expirydate"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mandatoryTrainingInstance?.staff}">
				<li class="fieldcontain">
					<span id="staff-label" class="property-label"><g:message code="mandatoryTraining.staff.label" default="Staff" /></span>
					
						<span class="property-value" aria-labelledby="staff-label"><g:link controller="staff" action="show" id="${mandatoryTrainingInstance?.staff?.id}">${mandatoryTrainingInstance?.staff?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:mandatoryTrainingInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${mandatoryTrainingInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
