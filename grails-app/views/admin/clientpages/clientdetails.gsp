<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta name="layout" content="main">
    <title>Client Details</title>
    <r:require modules="bootstrap" />
  </head>
  <body class="body">
      <div class="container">
        <br>
        <br>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
            <div class="row">
                <div class="col-xs-3">
                    <g:render template="/shared/adminsecondarymenu"/>
                </div>
                <div class="col-xs-9">
                    <fieldset>
                        <legend>Client Details</legend>
                        <ol class="property-list client">
			
				<g:if test="${clientInstance?.clientname}">
				<li class="fieldcontain">
					<span id="clientname-label" class="property-label"><g:message code="client.clientname.label" default="Clientname" /></span>
					
						<span class="property-value" aria-labelledby="clientname-label"><g:fieldValue bean="${clientInstance}" field="clientname"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${clientInstance?.telephonenumber}">
				<li class="fieldcontain">
					<span id="telephonenumber-label" class="property-label"><g:message code="client.telephonenumber.label" default="Telephonenumber" /></span>
					
						<span class="property-value" aria-labelledby="telephonenumber-label"><g:fieldValue bean="${clientInstance}" field="telephonenumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${clientInstance?.contactemail}">
				<li class="fieldcontain">
					<span id="contactemail-label" class="property-label"><g:message code="client.contactemail.label" default="Contactemail" /></span>
					
						<span class="property-value" aria-labelledby="contactemail-label"><g:fieldValue bean="${clientInstance}" field="contactemail"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${clientInstance?.address}">
				<li class="fieldcontain">
					<span id="address-label" class="property-label"><g:message code="client.address.label" default="Address" /></span>
					
						<span class="property-value" aria-labelledby="address-label"><g:fieldValue bean="${clientInstance}" field="address"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${clientInstance?.owner}">
				<li class="fieldcontain">
					<span id="owner-label" class="property-label"><g:message code="client.owner.label" default="Owner" /></span>
					
						<span class="property-value" aria-labelledby="owner-label"><g:fieldValue bean="${clientInstance}" field="owner"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${clientInstance?.contactperson}">
				<li class="fieldcontain">
					<span id="contactperson-label" class="property-label"><g:message code="client.contactperson.label" default="Contactperson" /></span>
					
						<span class="property-value" aria-labelledby="contactperson-label"><g:fieldValue bean="${clientInstance}" field="contactperson"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${clientInstance?.county}">
				<li class="fieldcontain">
					<span id="county-label" class="property-label"><g:message code="client.county.label" default="County" /></span>
					
						<span class="property-value" aria-labelledby="county-label"><g:fieldValue bean="${clientInstance}" field="county"/></span>
					
				</li>
				</g:if>
			
			</ol>
                        <span class="btn btn-danger" style="color: white"><g:link controller="admin" action="deleteclient" params="[id:clientInstance.id]">Delete</g:link></span>
                        <span class="btn btn-info"><g:link action="editclient" resource="${clientInstance}">Edit/Update</g:link></span>
                    </fieldset>
                </div>
            </div>
      </div>
  </body>
</html>
