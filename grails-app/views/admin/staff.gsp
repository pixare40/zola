<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page import="com.zola.shyft.Staff" %>

<html>
  <head>
    <meta name="layout" content="main">
    <title>Staff</title>
    <r:require modules="bootstrap" />
  </head>
  <body class="body">
      <div class="container">
        <br>
        <br>
        <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
        </g:if>
                <div class="row">
                    <div class="col-xs-5">
                    </div>
                    <div class="col-xs-7">
                        <g:if test="${staffInstanceCount>0}">
                            <table class="table table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <g:sortableColumn property="candidatename" title="Employee Name"/>
                                        <g:sortableColumn property="email" title="Enail" />
                                        <g:sortableColumn property="contactnumber" title="Phone Number"/>
                                        <th>View Staff Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <g:each in="${staffInstanceList}" status="i" var="staffInstance">
                                        <tr>
                                            <td>${fieldValue(bean:staffInstance, field:"candidatename")}</td>
                                            <td>${fieldValue(bean:staffInstance, field:"email")}</td>
                                            <td>${fieldValue(bean:staffInstance, field:"contactnumber")}</td>
                                            <td><g:link action="staffdetails" id="${staffInstance.id}">Details
                                            <span class="glyphicon glyphicon-circle-arrow-right"></span></g:link>
                                            </td>
                                        </tr>
                                        </g:each>
                                </tbody>
                            </table>
                           <div class="pagination">
                                <g:paginate total="${staffInstanceCount ?: 0}" />
                            </div>
                            </g:if>
                        <g:else>
                            <h1>No Records Found</h1>
                            </g:else>
                    </div>
                </div>
      </div>
  </body>
</html>
