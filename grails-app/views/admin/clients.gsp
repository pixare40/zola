<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page import="com.zola.shyft.Client" %>

<html>
  <head>
    <meta name="layout" content="main">
    <title>Staff</title>
    <r:require modules="bootstrap" />
  </head>
  <body class="body">
      <div class="container">
        <br>
        <br>
        <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
        </g:if>
                <div class="row">
                    <div class="col-xs-4">
                    </div>
                    <div class="col-xs-8">
                        <g:if test="${clientInstanceCount>0}">
                            <table class="table table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <g:sortableColumn property="clientname" title="Client Name"/>
                                        <g:sortableColumn property="contactemail" title="Contact Email" />
                                        <g:sortableColumn property="telephonenumber" title="Phone Number"/>
                                        <th>View Client Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <g:each in="${clientInstanceList}" status="i" var="clientInstance">
                                        <tr>
                                            <td>${fieldValue(bean:clientInstance, field:"clientname")}</td>
                                            <td>${fieldValue(bean:clientInstance, field:"contactemail")}</td>
                                            <td>${fieldValue(bean:clientInstance, field:"telephonenumber")}</td>
                                            <td>
                                            <g:link action="clientdetails" id="${clientInstance.id}">Details
                                            <span class="glyphicon glyphicon-circle-arrow-right"></span></g:link>
                                                
                                            </td>
                                        </tr>
                                        </g:each>
                                </tbody>
                            </table>
                           <ul class="pagination">
                                <g:paginate total="${staffInstanceCount ?: 0}" />
                            </ul>
                            </g:if>
                        <g:else>
                            <h1>No Records Found</h1>
                            </g:else>
                    </div>
                </div>
      </div>
  </body>
</html>
