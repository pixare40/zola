<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta name="layout" content="main">
    <title>Sample title</title>
    <r:require modules="bootstrap" />
  </head>
  <body class="body">
      <div class="container">
        <br>
        <br>
        <div class="col-xs-3">
        </div>
        <div class="col-xs-8">
            <div class="property-list">
                <input type="text" id="search_clients" class="typeahead" placeholder="Client" />
                <g:javascript src="typeahead.bundle.js"></g:javascript>
                 <g:javascript>
                     var substringMatcher = function(strs) {
                    return function findMatches(q, cb) {
                    var matches, substrRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function(i, str) {
                    if (substrRegex.test(str)) {
                    // the typeahead jQuery plugin expects suggestions to a
                    // JavaScript object, refer to typeahead docs for more info
                    matches.push({ value: str });
                    }
                    });

                    cb(matches);
                    };
                    };
        var users = [<g:each in="${clientlist}" var="u">'${u.clientname}(${u.county})',</g:each>]
        
        $('.typeahead').typeahead({
            minLength:1, 
            hint:true,  
            highlight: true
        },
        {
            name:'clients',
            displayKey:'value',
            source:substringMatcher(users)});
    </g:javascript>
            </div>
        </div>
      </div>
  </body>
</html>
