<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page import="com.zola.shyft.*" %>

<html>
  <head>
    <meta name="layout" content="main">
    <title>Administrator Dashboard</title>
    <gvisualization:apiImport/>
    <r:require modules="bootstrap" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'custom.styles.css')}" type="text/css">
  </head>
  <body class="body">
      <div class="container-fluid">
          <g:render template="/shared/menu" />
          <br>
          
          <g:if test="${flash.message}">
              <div class="message" role="status">${flash.message}</div>
          </g:if>
<div class="row">
          <div class="col-xs-4">
<fieldset>
    <legend class="admin-title">SHIFTS</legend>
    <div class="row-fluid" style="display: block">
        <div>
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>Total for...</th>
                        <th>Shifts</th>
                        <th>Hours</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Today</td>
                        <td>${shiftstoday}</td>
                        <td>${shifthourstoday}</td>
                    </tr>
                    <tr>
                        <td>This Week</td>
                        <td>${shiftsthisweek}</td>
                        <td>${shifthoursthisweek}</td>
                    </tr>
                    <tr>
                        <td>This Month</td>
                        <td>${shiftsthismonth}</td>
                        <td>${shifthoursthismonth}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <br>
        <div class="pull-right"><g:link controller="shift" action="index">View Shift Data &raquo;</g:link></div>
    </div>
    </fieldset>
        <br>
        <fieldset>
            <legend class="admin-title">STAFF</legend>
        <div class="row-fluid" style="display: block">
        <div class="admin-details">Carers:<span style="float:right">${carersnumber}</span></div>
        <div class="admin-details">Support Workers:<span style="float:right">${supportnumber}</span></div>
        <div class="admin-details">Nurses:<span style="float:right">${nursenumber}</span></div>
        <br>
        <div class="pull-right"><g:link controller="staff" action="index">View Staff Data &raquo;</g:link></div>
    </div>
        </fieldset>
        <br>
        <fieldset>
            <legend class="admin-title">CLIENTS</legend>
        <div class="row-fluid" style="display: block">
        <div class="admin-details">Total Clients Registered:<span style="float:right">${clientnumber}</span></div>
        
        <br>
        <div class="pull-right"><g:link controller="client" action="index">View Client Data &raquo;</g:link></div>
    </div>
        </fieldset>
        <br>
    </div>
    <div class="col-xs-8">

              <legend class="admin-title">TRENDS</legend>
              <div>
                      <script type="text/javascript">
                         var data = ${raw(shiftsjson)}
$(function () {
    $('#shifts-breakdown').highcharts({
        title: {
            text: 'Shifts BreakDown',
            x: -20 //center
        },
        subtitle: {
            text: 'Based on Monthly Averages (Calculated at end of Day)',
            x: -20
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Number of Shifts'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Shifts',
            data: ${shiftsstring}
        }]
    });
});
		</script>
                 <g:javascript src="highcharts.js"></g:javascript>
    <g:javascript src="modules/exporting.js"></g:javascript>
    <div class="row-fluid" id="shifts-breakdown"></div>
    <div></div>
              </div>
              
          </div>
</div>
      </div>
  </body>
</html>
