<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page contentType="text/html;charset=UTF-8" %>

<html>
  <head>
    <meta name="layout" content="main">
    <title>Staff Details</title>
    <r:require modules="bootstrap" />
  </head>
  <body class="body">
      <div class="container">
        <br>
        <br>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
            <div class="row">
                <div class="col-xs-3">
                    <g:render template="/shared/adminsecondarymenu"/>
                </div>
                <div class="col-xs-9">
                    <fieldset>
                        <legend>Staff Details</legend>
                    </fieldset>
                </div>
            </div>
      </div>
  </body>
</html>
