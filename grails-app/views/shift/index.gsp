
<%@ page import="com.zola.shyft.Shift" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'shift.label', default: 'Shift')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
                <r:require modules="bootstrap"/>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'custom.styles.css')}" type="text/css">
	</head>
	<body>
		<g:render template="/shared/menu" />
		<div class="container">
                    <div class="col-xs-3">
                        <g:render template="shiftactions"/>
                    </div>
            <div class="col-xs-8">
                        <fieldset>
                            <legend>Shift List</legend>
		<div id="list-shift" class="content scaffold-list" role="main">
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-hover">
			<thead>
					<tr>
					
						<g:sortableColumn property="shiftdate" title="Shift Date" />
					
						<g:sortableColumn property="shifttype" title="Shift Type" />
					
						<th><g:message code="shift.home.label" default="Home" /></th>
					
						<th>Staff</th>
					
						<g:sortableColumn property="status" title="${message(code: 'shift.status.label', default: 'Status')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${shiftInstanceList}" status="i" var="shiftInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${shiftInstance.id}">${fieldValue(bean: shiftInstance, field: "shiftdate")}</g:link></td>
					
						<td>${fieldValue(bean: shiftInstance, field: "shifttype")}</td>
					
						<td>${fieldValue(bean: shiftInstance, field: "home")}</td>
					
						<td>${fieldValue(bean: shiftInstance, field: "staffassigned")}</td>
					
						<td>${fieldValue(bean: shiftInstance, field: "status")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
                        <g:if test="${shiftInstanceCount > 10}">
			<ul class="pagination">
				<g:paginate total="${shiftInstanceCount ?: 0}" />
			</ul>
                        </g:if>
		</div>
                 </fieldset>
            </div>
                </div>
	</body>
</html>
