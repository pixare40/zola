<%@ page import="com.zola.shyft.Shift" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'shift.label', default: 'Shift')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
                <r:require modules="bootstrap"/>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'custom.styles.css')}" type="text/css">
	</head>
	<body>
            <g:render template="/shared/menu" />
		<a href="#edit-shift" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="container">
                    <div class="col-xs-3">
                        <g:render template="shiftactions"/>
                    </div>
            <div class="col-xs-8">
                <fieldset>
                    <legend>Edit Shift</legend>
		<div id="edit-shift" class="content scaffold-edit" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${shiftInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${shiftInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form url="[resource:shiftInstance, action:'update']" method="PUT" >
				<g:hiddenField name="version" value="${shiftInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
				</fieldset>
			</g:form>
		</div>
                </fieldset>
                </div>
            </div>
	</body>
</html>
