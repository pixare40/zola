
<%@ page import="com.zola.shyft.Shift" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'shift.label', default: 'Shift')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
                <r:require modules="bootstrap"/>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'custom.styles.css')}" type="text/css">
	</head>
	<body>
            <g:render template="/shared/menu" />
		<a href="#show-shift" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="container">
                    <div class="col-xs-3">
                        <g:render template="shiftactions"/>
                    </div>
            <div class="col-xs-8">
                <fieldset>
                    <legend>Shift Information</legend>
		<div id="show-shift" class="content scaffold-show" role="main">
			
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list shift">
			
				<g:if test="${shiftInstance?.shiftdate}">
				<li class="fieldcontain">
					<span id="shiftdate-label" class="property-label"><g:message code="shift.shiftdate.label" default="Shiftdate" /></span>
					
						<span class="property-value" aria-labelledby="shiftdate-label"><g:fieldValue bean="${shiftInstance}" field="shiftdate"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${shiftInstance?.shifttype}">
				<li class="fieldcontain">
					<span id="shifttype-label" class="property-label"><g:message code="shift.shifttype.label" default="Shifttype" /></span>
					
						<span class="property-value" aria-labelledby="shifttype-label"><g:fieldValue bean="${shiftInstance}" field="shifttype"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${shiftInstance?.home}">
				<li class="fieldcontain">
					<span id="home-label" class="property-label"><g:message code="shift.home.label" default="Home" /></span>
					
						<span class="property-value" aria-labelledby="home-label"><g:link controller="client" action="show" id="${shiftInstance?.home?.id}">${shiftInstance?.home?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${shiftInstance?.staffassigned}">
				<li class="fieldcontain">
					<span id="staffassigned-label" class="property-label"><g:message code="shift.staffassigned.label" default="Staffassigned" /></span>
					
						<span class="property-value" aria-labelledby="staffassigned-label"><g:link controller="staff" action="show" id="${shiftInstance?.staffassigned?.id}">${shiftInstance?.staffassigned?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${shiftInstance?.status}">
				<li class="fieldcontain">
					<span id="status-label" class="property-label"><g:message code="shift.status.label" default="Status" /></span>
					
						<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${shiftInstance}" field="status"/></span>
					
				</li>
				</g:if>
                                
                                <g:if test="${shiftInstance?.shifthours}">
				<li class="fieldcontain">
					<span id="shifthours-label" class="property-label">Hours</span>
					
						<span class="property-value" aria-labelledby="shifthours-label"><g:fieldValue bean="${shiftInstance}" field="shifthours"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:shiftInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${shiftInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
                </fieldset>
                </div>
                </div>
	</body>
</html>
