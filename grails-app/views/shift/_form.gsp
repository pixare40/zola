<%@ page import="com.zola.shyft.Shift" %>



<div class="fieldcontain ${hasErrors(bean: shiftInstance, field: 'shiftdate', 'error')} required">
	<label for="shiftdate">
		<g:message code="shift.shiftdate.label" default="Shiftdate" />
		<span class="required-indicator">*</span>
	</label>
        <joda:dateField name="shiftdate" value="${shiftInstance?.shiftdate}" /> Format: dd/mm/yyyy

        

</div>

<div class="fieldcontain ${hasErrors(bean: shiftInstance, field: 'shifttype', 'error')} required">
	<label for="shifttype">
		<g:message code="shift.shifttype.label" default="Shifttype" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="shifttype" from="${shiftInstance.constraints.shifttype.inList}" required="" value="${shiftInstance?.shifttype}" valueMessagePrefix="shift.shifttype"/>

</div>

<div class="fieldcontain ${hasErrors(bean: shiftInstance, field: 'home', 'error')} required">
	<label for="home">
		<g:message code="shift.home.label" default="Home" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="home" name="home.id" from="${com.zola.shyft.Client.list()}" optionKey="id" required="" value="${shiftInstance?.home?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: shiftInstance, field: 'staffassigned', 'error')} ">
	<label for="staffassigned">
		<g:message code="shift.staffassigned.label" default="Staffassigned" />
		
	</label>
	<g:select id="staffassigned" name="staffassigned.id" from="${com.zola.shyft.Staff.list()}" optionKey="id" value="${shiftInstance?.staffassigned?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: shiftInstance, field: 'status', 'error')} required">
	<label for="status">
		<g:message code="shift.status.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="status" from="${shiftInstance.constraints.status.inList}" required="" value="${shiftInstance?.status}" valueMessagePrefix="shift.status"/>

</div>

