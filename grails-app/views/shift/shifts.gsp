<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'shift.label', default: 'Shift')}" />
		<title>${pageTitle}</title>
                <r:require modules="bootstrap"/>
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'custom.styles.css')}" type="text/css">
	</head>
	<body>
		<g:render template="/shared/menu" />
		<div class="container">
                    <div class="col-xs-3">
                        <g:render template="shiftactions"/>
                    </div>
            <div class="col-xs-8">
                        <fieldset>
                            <legend>${pageTitle}</legend>
		<div id="list-shift" class="content scaffold-list" role="main">
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-hover">
			<thead>
					<tr>
					
						<g:sortableColumn property="shiftdate" title="${message(code: 'shift.shiftdate.label', default: 'Shiftdate')}" />
					
						<g:sortableColumn property="shifttype" title="${message(code: 'shift.shifttype.label', default: 'Shifttype')}" />
					
						<th><g:message code="shift.home.label" default="Home" /></th>
					
						<th><g:message code="shift.staffassigned.label" default="Staffassigned" /></th>
					
						<g:sortableColumn property="status" title="${message(code: 'shift.status.label', default: 'Status')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${shiftInstanceList}" status="i" var="shiftInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${shiftInstance.id}">${fieldValue(bean: shiftInstance, field: "shiftdate")}</g:link></td>
					
						<td>${fieldValue(bean: shiftInstance, field: "shifttype")}</td>
					
						<td>${fieldValue(bean: shiftInstance, field: "home")}</td>
					
						<td>${fieldValue(bean: shiftInstance, field: "staffassigned")}</td>
					
						<td>${fieldValue(bean: shiftInstance, field: "status")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
                        <g:if test="${shiftInstanceCount > 10}">
			<ul class="pagination">
				<g:paginate total="${shiftInstanceCount ?: 0}" />
			</ul>
                        </g:if>
		</div>
                 </fieldset>
            </div>
                </div>
	</body>
</html>
