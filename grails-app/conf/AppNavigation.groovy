/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kegara
 */

navigation={
   
    
    staffactions{
        Create (controller:'staff', action:'create', titleText:'Add New Staff')
        Freestaff (controller:'staff', action:'freestaff',titleText:'View Free Staff')
    }
    
    clientaction{
        Create (controller:'client', action:'create', titleText:'Add New Client')
        viewall (controller:'client', action:'index', titleText:'View All Clients')
    }
    
    clientsensitive{
        ViewFormerStaff (controller:'client',action:'formerstaff',titleText:'Staff Who Have Worked Here')
        ViewPastShifts (controller:'client', action:'pastshifts',titleText:'Past Shifts')
    }
    
    shiftactions{
        Shifts (controller:'shift', action:'index', titleText:'All Shifts')
        Create (controller:'shift', action:'create', titleText:'Create New Shift')
        Unassigned (controller:'shift', action:'unassigned',titleText:'View Unassigned Shifts')
    }
    
     admin{
        Dashboard (controller:'admin', action:'index', titleText:'Admin Dashboard')
        Staff (controller:'staff', action:'index',titleText:'Staff Management')
        Shift (controller:'shift', action:'index', titleText:'Shift Management')
        Client (controller:'client', action:'index', titleText:'Client Management')
        //Payroll (controller:'payroll', action:'index', titleText:'Payroll Management')
        Advanced (controller:"home", action:'appdetails', titleText:'Advanced Tasks')
        Messaging (controller:"admin", action:'messaging', titleText:'Messaging & Notifications')
    }
    
    worker{
        Dashboard (controller:'admin', action:'index', titleText:'Dashboard')
        Shifts (controller:'worker', action:'myshifts', titleText:'My Shifts')
        Messages (controller:'worker', action:'mymessages', titleText:'My Messages')
    }
}

