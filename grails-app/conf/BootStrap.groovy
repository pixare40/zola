
import com.zola.shyft.*
import org.joda.time.DateTime
import org.joda.time.*
import org.jadira.usertype.dateandtime.joda.*
import grails.util.Environment


class BootStrap {

    def mailService
    def init = { servletContext ->
        
        if(Environment.current == Environment.PRODUCTION){
            
             def adminRole = Role.findByAuthority('ROLE_ADMIN')?:new Role(authority:'ROLE_ADMIN').save(failOnError:true)
        def staffRole = Role.findByAuthority('ROLE_STAFF')?:new Role(authority:'ROLE_STAFF').save(failOnError:true)
            
        mailService.sendMail {     
            to "kabajiegara@live.com"     
            subject "Zola Staffing Platform"     
            body 'Application Powering Up' 
          }
          println "Application Powering up, Test email sent."
          
            def secAdmin1 = User.findByEmail('ken@kkhealthcare.co.uk')?: new User(
            username:'kenkamiri',
            password:'wirawira10',
            email:'ken@kkhealthcare.co.uk'
        ).save(flush:true)
        
         def secAdmin2 = User.findByEmail('leah@kkhealthcare.co.uk')?: new User(
            username:'leah',
            password:'wirawira10',
            email:'leah@kkhealthcare.co.uk'
        ).save(flush:true)
        
            def secAdmin3 = User.findByEmail('kabzegara@gmail.com')?: new User(
            username:'kabajiegara',
            password:'wirawira10',
            email:'kabzegara@gmail.com'
        ).save(flush:true)
        
            if(!secAdmin1.authorities.contains(adminRole)){
            UserRole.create secAdmin1, adminRole
        }
        
            if(!secAdmin2.authorities.contains(adminRole)){
            UserRole.create secAdmin2, adminRole
        }
        
            if(!secAdmin3.authorities.contains(adminRole)){
            UserRole.create secAdmin3, adminRole
        }
        }
        if(Environment.current == Environment.DEVELOPMENT){
        //Application Roles
       
        def adminRole = Role.findByAuthority('ROLE_ADMIN')?:new Role(authority:'ROLE_ADMIN').save(failOnError:true)
        def staffRole = Role.findByAuthority('ROLE_STAFF')?:new Role(authority:'ROLE_STAFF').save(failOnError:true)
        
        //Application Users
         def secAdmin1 = User.findByEmail('ken@kkhealthcare.co.uk')?: new User(
            username:'kenkamiri',
            password:'wirawira10',
            email:'ken@kkhealthcare.co.uk'
        ).save(flush:true)
        
         def secAdmin2 = User.findByEmail('leah@kkhealthcare.co.uk')?: new User(
            username:'leah',
            password:'wirawira10',
            email:'leah@kkhealthcare.co.uk'
        ).save(flush:true)
            
        def secAdmin = new User(
            username:'alenemoss',
            password:'wirawira10',
            email:'alene.moss@gmail.com'
        ).save(flush:true)
        
        def secStaff = new Staff(
            username : 'testuser',
            password: 'miriam12',
            email : 'kabajiegara@live.com',
            avatar : null,
            stafftype : 'Nurse',
            candidatename : 'Kabaji Egara',
            knownas : 'Kabaji',
            address : '72 Ruaraka',
            contactnumber : '0720752520',
            dateofbirth : new Date(),
            dateoflastDBScheck : new Date(),
            dbsnumber : '37434',
            eligibletoworkintheuk : true,
            passport : true,
            visauptodate : 'Yes',
            confirmationofidentity : true,
            proofofaddress : true,
            referencesreceived : true,
            drivinglicenseandinsurance : true,
            allrequiredpaperwork : true,
            mandatorytraininguptodate : true,
            registeredunderdisabilityact : true,
            staffhappy : true
        ).save(failOnError:true)
        
        def democlient = new Client(
            clientname:'King Home',
            telephonenumber:'07413346938',
            contactemail:'kabajiegara@gmail.com',
            address:'72 Ruaraka, nairobi Kenya',
            owner:'Worldwide Healthcare coop',
            contactperson:'Mr Alfred Imply',
            county:'Bedfordshire'
        ).save(flush:true)
        
        //Assigning Roles to Users
        if(!secAdmin.authorities.contains(adminRole)){
            UserRole.create secAdmin, adminRole
        }
        
            if(!secAdmin1.authorities.contains(adminRole)){
            UserRole.create secAdmin1, adminRole
        }
        
            if(!secAdmin2.authorities.contains(adminRole)){
            UserRole.create secAdmin2, adminRole
        }
        
            if(!secStaff.authorities.contains(staffRole)){
            UserRole.create secStaff, staffRole
        }
                
        def yeardatademo = new YearShiftData(
            year:2014
        ).save(flush:true)
        
        def shift = new Shift(
            shiftdate:new LocalDate(),
            shifttype:"Early",
            home:democlient,
            staffassigned:secStaff,
            status:"Booked",
            shifthours:6
        ).save(flush:true)
        
        def shift1 = new Shift(
            shiftdate:new LocalDate().minusDays(20),
            shifttype:"Early",
            home:democlient,
            staffassigned:secStaff,
            status:"Booked",
            shifthours:10
        ).save(flush:true)

        def shift2 = new Shift(
            shiftdate:new LocalDate().minusDays(5),
            shifttype:"Early",
            home:democlient,
            status:"Booked",
            shifthours:7
        ).save(flush:true)
        
            
        def message1 = new Message(
            recipient : secStaff,
            sender : secAdmin1,
            title : 'Welcome To KK HealthCare',
            message : 'This is our new IT infrastructure'
        ).save(failOnError:true)
        
        }
        
    }
    def destroy = {
    }
}
