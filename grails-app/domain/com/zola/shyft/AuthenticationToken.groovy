package com.zola.shyft

class AuthenticationToken {
    
    String username
    String token

    static constraints = {
    }
}
