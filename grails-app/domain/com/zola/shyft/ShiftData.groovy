package com.zola.shyft

import org.joda.time.DateTime
import org.joda.time.*
import org.jadira.usertype.dateandtime.joda.*

class ShiftData {
    
    LocalDate date
    int totalshifts

    static constraints = {
        date nullable:false
        totalshifts nullable:false
    }
    
    public int getTotalShifts(){
        return totalshifts
    }
}
