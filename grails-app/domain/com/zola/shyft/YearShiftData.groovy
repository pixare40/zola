package com.zola.shyft

class YearShiftData {
    
    int year
    int january
    int february
    int march
    int april
    int may
    int june
    int july
    int august
    int september
    int october
    int november
    int december

    static constraints = {
        year nullable:false, blank:false
        january default:0, nullable:true
        february default:0, nullable:true
        march default:0, nullable:true
        april default:0, nullable:true
        may default:0, nullable:true
        june default:0, nullable:true
        july default:0, nullable:true
        august default:0, nullable:true
        september default:0, nullable:true
        october default:0, nullable:true
        november default:0, nullable:true
        december default:0, nullable:true
    }
}
