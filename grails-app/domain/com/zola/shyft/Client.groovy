package com.zola.shyft

class Client {
    String clientname
    String telephonenumber
    String contactemail
    String address
    String owner
    String contactperson
    String county
    Date dateCreated
    Date lastUpdated

    static constraints = {
        clientname nullable:false
        telephonenumber nullable:false
        contactemail nullable:false
        address nullable:false
        owner nullable:true
        contactperson nullable:false
        county nullable:false
    }
    
    String toString(){
        return "${clientname}(${county})"
    }
}
