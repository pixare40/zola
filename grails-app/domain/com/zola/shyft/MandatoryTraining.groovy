package com.zola.shyft

import org.joda.time.LocalDate

class MandatoryTraining {
    String training
    Boolean undertaken
    Date dateCreated
    Date lastUpdated
    LocalDate datecompleted
    LocalDate expirydate
    
    static belongsTo= [staff:Staff]

    static constraints = {
        training nullbale:false, blank:false
        undertaken nullable:false, blank:false
        datecompleted nullable:true, blank:true
        expirydate nullable:true, blank:false
    }
    
    String toString(){
        return "${training}"
    }
}
