package com.zola.shyft


class Message {
    
    Staff recipient
    User sender
    String title
    String message
    Date dateCreated
    Date lastUpdated
    boolean readproperty = false
    
    static constraints = {
        recipient nullable:false, blank:false
        sender nullable:true, blank:true
        title nullable:false, blank:false
        message nullable:false, blank:false
    }
}
