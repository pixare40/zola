package com.zola.shyft

class PayPackage {
    
    Double sleeprateWD
    Double sleeprateWE
    Double sleeprateBH
    Double earlyrateWD
    Double earlyrateWE
    Double earlyrateBH
    Double laterateWD
    Double laterateWE
    Double laterateBH
    Double wakenightrateWD
    Double wakenightrateWE
    Double wakenightrateBH
    Double daycarerateWD
    Double daycarerateWE
    Double daycarerateBH
    Double longdayrateWD
    Double longdayrateWE
    Double longdayrateBH
    
    static belongsTo=[staff:Staff]

    static constraints = {
        sleeprateWD nullable:true
        sleeprateWE nullable:true
        sleeprateBH nullable:true
        earlyrateWD nullable:true
        earlyrateWE nullable:true
        earlyrateBH nullable:true
        laterateWD nullable:true
        laterateWE nullable:true
        laterateBH nullable:true
        wakenightrateWD nullable:true
        wakenightrateWE nullable:true
        wakenightrateBH nullable:true
        daycarerateWD nullable:true
        daycarerateWE nullable:true
        daycarerateBH nullable:true
        longdayrateWD nullable:true
        longdayrateWE nullable:true
        longdayrateBH nullable:true
    }
}
