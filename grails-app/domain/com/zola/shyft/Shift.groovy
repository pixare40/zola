package com.zola.shyft

import org.joda.time.*
import org.joda.time.contrib.hibernate.*
import org.jadira.usertype.dateandtime.joda.*

class Shift {
    
    LocalDate shiftdate
    String shifttype
    Client home
    Staff staffassigned
    String status
    int shifthours
    Date dateCreated
    Date lastUpdated

    static constraints = {
        shiftdate nullable:false
        shifttype nullable:false,inList:["Early", "Daycare", "Late", "Longday", "WakeNight", "Sleep"]
        home nullable:false
        staffassigned nullable:true
        status nullable:false, inList:["Booked","Cancel By Staff","Cancel By Home","Not Booked Yet","Unable To Cover","Provisional"]
        shifthours nullable:true
    }
}
