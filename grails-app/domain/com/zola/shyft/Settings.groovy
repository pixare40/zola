package com.zola.shyft

class Settings {
    int totalShiftsThisMonth
    int totalShiftsLastMonth
    int totalShifts2Months
    int totalShifts3Months
    int totalShifts4Months

    static constraints = {
        totalShiftsThisMonth nullable:false
        totalShiftsLastMonth nullable:false
        totalShifts2Months nullable:false
        totalShifts3Months nullable:false
        totalShifts4Months nullable:false
    }
}
