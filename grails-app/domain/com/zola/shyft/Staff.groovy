package com.zola.shyft

class Staff extends User {
    
    String stafftype
    String candidatename
    String knownas
    String address
    String contactnumber
    String avatar
    Date dateofbirth
    Date dateoflastDBScheck
    String dbsnumber
    Date dateCreated
    Date lastUpdated
    //Checks on staff
    Boolean eligibletoworkintheuk
    Boolean passport
    String visauptodate
    Boolean confirmationofidentity
    Boolean proofofaddress
    Boolean referencesreceived
    Boolean drivinglicenseandinsurance
    Boolean allrequiredpaperwork
    Boolean mandatorytraininguptodate
    Boolean registeredunderdisabilityact
    Boolean staffhappy
    PayPackage paypackage
    
    //Trainings Undertaken
    
    static hasMany = [trainings:MandatoryTraining,shifts:Shift]

    static constraints = {
        avatar(nullable:true)
        stafftype nullable:false,blank:false, inList:['Nurse','Support Worker','Carer','Home Care','Live-in Care']
        candidatename nullable:false, blank:false
        knownas nullable:true
        address nullable:false
        contactnumber nullable:false
        dateofbirth nullable:false
        dateoflastDBScheck nullable:false
        dbsnumber nullable:false
        eligibletoworkintheuk nullable:false
        passport nullable:false
        visauptodate nullable:false
        confirmationofidentity nullable:false
        proofofaddress nullable:false
        referencesreceived nullable:false
        drivinglicenseandinsurance nullable:false
        allrequiredpaperwork nullable:false
        mandatorytraininguptodate nullable:false
       registeredunderdisabilityact nullable:false
        staffhappy nullable:false
        paypackage nullable:true
        
    }
    
    String toString(){
        return "${candidatename} ${stafftype}"
    }
}
