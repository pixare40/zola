package com.zola.shyft

import grails.transaction.Transactional
import org.springframework.web.multipart.MultipartFile
import org.codehaus.groovy.grails.web.context.ServletContextHolder


@Transactional
class FileUploadrService {

    def uploadProfilePicture(int userid, String filename) {

    }
    
    def uploadFile(MultipartFile file, String name, String destinationDirectory){
        def servletContext = ServletContextHolder.servletContext
        def storagePath = servletContext.getRealPath(destinationDirectory)
        
        //Create Storage path directory if it does not exist
        def storagePathDirectory = new File(storagePath)
        if(!storagePathDirectory.exists()){
            print "CREATING DIRECTORY ${storagePath}: "
            if(storagePathDirectory.mkdirs()){
                print "SUCCESS"
            }
            else{
                print "FAILED"
            }
        }
        
        //Store File
        if(!file.isEmpty()){
            file.transferTo(new File("${storagePath}/${name}"))
            println "Saved File: ${storagePath}/${name}"
            return "${storagePath}/${name}"
        }
        else{
            println "File ${file.inspect()} was empty"
            return null
        }
        
    }
    
    def getFileUrl(int userid, String filename){
        
    }
}
