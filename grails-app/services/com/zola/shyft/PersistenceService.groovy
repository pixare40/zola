package com.zola.shyft

import grails.transaction.Transactional
import org.joda.time.DateTime
import org.joda.time.*
import org.jadira.usertype.dateandtime.joda.*

@Transactional
class PersistenceService {

    def getStaff() {
        def stafflist = Staff.list()
        return stafflist
    }
    
    def getClients(){
        def clientlist = Client.list()
        return clientlist
    }
    
    def deleteClient(Client clientInstance){
        clientInstance.delete flush:true
    }
    
    def deleteStaff(Staff staffInstance){
        staffInstance.delete flush:true
    }
    
    def getPastShiftsByMonth(){
        def shiftarray = new int[12]
        DateTime currentdate = new DateTime();
        def currentyear = currentdate.getYear();
        def currentyearshiftdata = YearShiftData.findByYear(currentyear)
        shiftarray[0] = currentyearshiftdata.january
        shiftarray[1] = currentyearshiftdata.february
        shiftarray[2] = currentyearshiftdata.march
        shiftarray[3] = currentyearshiftdata.april
        shiftarray[4] = currentyearshiftdata.may
        shiftarray[5] = currentyearshiftdata.june
        shiftarray[6] = currentyearshiftdata.july
        shiftarray[7] = currentyearshiftdata.august
        shiftarray[8] = currentyearshiftdata.september
        shiftarray[9] = currentyearshiftdata.october
        shiftarray[10] = currentyearshiftdata.november
        shiftarray[11] = currentyearshiftdata.december
        
        return shiftarray
        
    }
    
    def getNumberofCarers(){
        def carerslist = Staff.findAll{
            stafftype == "Carer"
        }
        def numberofcarers = carerslist.size()
        return numberofcarers
    }
    
    def getNumberofSupportWorkers(){
        def supportworkers = Staff.findAll{
            stafftype == "Support Worker"
        }
        def numberofsupportworkers = supportworkers.size()
        return numberofsupportworkers
    }
    
    def getNumberofNurses(){
        def nurses = Staff.findAll{
            stafftype == "Nurse"
        }
        
        def numberofnurses = nurses.size()
        return numberofnurses
    }
    
    def getNumberOfClient(){
        def clients = Client.findAll().asList()
        def clientcount = clients.size()
        return clientcount
    }
    
    def getNumberOfShiftsToday(){
        def todayshifts = Shift.findAllByShiftdate(new LocalDate()).asList().size()
        return todayshifts
    }
    
    def getNumberOfShiftHoursToday(){
        def todayshifts = Shift.findAllByShiftdate(new LocalDate()).asList()
        int sum = 0
        for(int i=0;i<todayshifts.size();i++){
            sum = sum + todayshifts[i].shifthours
        }
        return sum
    }
    
    def getNumberOfShiftsThisWeek(){
        def currentdayoftheweek = new LocalDate().getDayOfWeek()
        def c = Shift.createCriteria()
        def results = c.list{
            between('shiftdate',new LocalDate().minusDays(currentdayoftheweek), new LocalDate())
        }
        return results.size()
    }
    
    def getNumberOfShiftHoursThisWeek(){
        def currentdayoftheweek = new LocalDate().getDayOfWeek()
        def c = Shift.createCriteria()
        def results = c.list{
            between('shiftdate',new LocalDate().minusDays(currentdayoftheweek), new LocalDate())
        }
        int sum = 0
        for(int i=0;i<results.size();i++){
            sum = sum + results[i].shifthours
        }
        return sum
    }
    
    def getNumberOfShiftsThisMonth(){
        def currentdayofthemonth = new LocalDate().getDayOfMonth()
        def c = Shift.createCriteria()
        def results = c.list{
            between('shiftdate', new LocalDate().minusDays(currentdayofthemonth),new LocalDate())
        }
        return results.size()
        
    }
    
    def getNumberOfShiftHoursThisMonth(){
        def currentdayofthemonth = new LocalDate().getDayOfMonth()
        def c = Shift.createCriteria()
        def results = c.list{
            between('shiftdate', new LocalDate().minusDays(currentdayofthemonth),new LocalDate())
        }
        int sum = 0
        for(int i=0;i<results.size();i++){
            sum = sum + results[i].shifthours
        }
        return sum
    }
    
    def getFreeStaff(){
        def todaysshifts = Shift.findAll{
            shiftdate == new LocalDate()
        }
        def assignedstaff = new ArrayList<Staff>()
        for (int i = 0;i<todaysshifts.size();i++){
            if(todaysshifts[i].staffassigned != null){
                def astaff = todaysshifts[i].staffassigned
                assignedstaff.add(astaff)
            }
        }
        println todaysshifts
        println assignedstaff
        def allstaff = Staff.findAll().asList()
        if(allstaff.size() == 0){
            println "allstaff tripped"
            return new ArrayList<Staff>()
        }
        if (assignedstaff.size() == 0){
            return allstaff
        }
        else{
            for(int i=0;i<assignedstaff.size();i++){
               for(int j=0;j<allstaff.size();j++){
                   if(allstaff[j].id == assignedstaff[i].id){
                       allstaff.remove(allstaff[j])
                   }
               }
            }
        }
        return allstaff
    }
    
    def getUnassignedShifts(){
        def shifts = Shift.findAll()
        def unassignedshifts = new ArrayList<Shift>()
        for (int i = 0;i<shifts.size();i++){
            if(shifts[i].staffassigned == null){
                unassignedshifts.add(shifts[i])
            }
        }
        
        return unassignedshifts
        
    }
    
    
}
