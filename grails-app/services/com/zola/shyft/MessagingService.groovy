package com.zola.shyft

import grails.transaction.Transactional

@Transactional
class MessagingService {

    def getAdminSentMessages(User adminuser) {
        def sentmessages = Message.findAll(sort:"dateCreated"){
            sender == adminuser
        }
        return sentmessages
    }
    
    def getStaffSentMessages(Staff staffuser){
        
    }
    
    def getStaffReceivedMessages(Staff staffuser){
        
    }
    
    def createNewMessage(User sender, Staff recipient, String subject, String message){
        def newmessage = new Message(
            sender:sender,
            recipient:recipient,
            title:subject,
            message:message
        ).save(flush:true)
        
        return newmessage
    }
}
