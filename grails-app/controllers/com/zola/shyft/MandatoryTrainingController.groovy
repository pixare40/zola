package com.zola.shyft



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MandatoryTrainingController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond MandatoryTraining.list(params), model:[mandatoryTrainingInstanceCount: MandatoryTraining.count()]
    }

    def show(MandatoryTraining mandatoryTrainingInstance) {
        respond mandatoryTrainingInstance
    }

    def create() {
        respond new MandatoryTraining(params)
    }

    @Transactional
    def save(MandatoryTraining mandatoryTrainingInstance) {
        if (mandatoryTrainingInstance == null) {
            notFound()
            return
        }

        if (mandatoryTrainingInstance.hasErrors()) {
            respond mandatoryTrainingInstance.errors, view:'create'
            return
        }

        mandatoryTrainingInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'mandatoryTraining.label', default: 'MandatoryTraining'), mandatoryTrainingInstance.id])
                redirect mandatoryTrainingInstance.staff
            }
            '*' { respond mandatoryTrainingInstance, [status: CREATED] }
        }
    }

    def edit(MandatoryTraining mandatoryTrainingInstance) {
        respond mandatoryTrainingInstance
    }

    @Transactional
    def update(MandatoryTraining mandatoryTrainingInstance) {
        if (mandatoryTrainingInstance == null) {
            notFound()
            return
        }

        if (mandatoryTrainingInstance.hasErrors()) {
            respond mandatoryTrainingInstance.errors, view:'edit'
            return
        }

        mandatoryTrainingInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'MandatoryTraining.label', default: 'MandatoryTraining'), mandatoryTrainingInstance.id])
                redirect mandatoryTrainingInstance
            }
            '*'{ respond mandatoryTrainingInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(MandatoryTraining mandatoryTrainingInstance) {

        if (mandatoryTrainingInstance == null) {
            notFound()
            return
        }

        mandatoryTrainingInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'MandatoryTraining.label', default: 'MandatoryTraining'), mandatoryTrainingInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'mandatoryTraining.label', default: 'MandatoryTraining'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
