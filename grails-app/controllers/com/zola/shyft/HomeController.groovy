package com.zola.shyft

import grails.plugin.springsecurity.SpringSecurityUtils

class HomeController {

    def index() {
        
        if(SpringSecurityUtils.ifAllGranted('ROLE_ADMIN')){
            flash.message = "Chart Data is calculated at the end of the day. Provided for analytics purposes. Not in Real Time"
            redirect controller:'Admin', action:'index'
        }
        
        else if(SpringSecurityUtils.ifAllGranted('ROLE_STAFF')){
            redirect controller:'Worker', action:'index'
        }
        else{
            redirect controller:'Login', action:'auth'
        }
    }
    def home(){
        render view:'index'
    }
    def appdetails(){
        render view:'/index'
    }
}
