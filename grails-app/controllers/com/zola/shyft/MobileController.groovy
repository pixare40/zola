package com.zola.shyft

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.rest.render.json.*
import grails.converters.*
import org.joda.time.*

class MobileController {
    
    def index() {
        
    }
    
    def getshifts(){
        def curauth = AuthenticationToken.findByToken(request.getHeader("x-auth-token"))
        def staffmember = Staff.findByUsername(curauth.username)
        def shifts = Shift.findAll(sort:'shiftdate'){
            staffassigned == staffmember
        }
        def pogoshift = new ArrayList()
        shifts.each{shift ->
            def staffshift = new StaffShift(shift.id,shift.home.clientname,shift.shiftdate, shift.shifttype, shift.shifthours)
            pogoshift.add(staffshift)
        }
        render pogoshift as JSON
    }
    
    def getmessages(){
        def curauth = AuthenticationToken.findByToken(request.getHeader("x-auth-token"))
        def staffmember = Staff.findByUsername(curauth.username)
        def mymessages = Message.findAll(sort:'dateCreated'){
            recipient == staffmember
        }
        def pogomessages = new ArrayList()
        mymessages.each{_message ->
            def newmessage = new StaffMessage(_message.id, _message.title, _message.message, _message.sender.username, _message.dateCreated)
            pogomessages.add(newmessage)
        }
        render pogomessages as JSON
    }
    
    def getmessage(Message messageInstance){
        render messageInstance as JSON
    }
    
    def getshift(Shift shiftInstance){
        def myshift = new StaffShift(shiftInstance.id,shiftInstance.home.clientname,shiftInstance.shiftdate, shiftInstance.shifttype, shiftInstance.shifthours)
        render myshift as JSON
    }
    
    def getprofile(){
        def curauth = AuthenticationToken.findByToken(request.getHeader("x-auth-token"))
        def staffmember = Staff.findByUsername(curauth.username)
        println staffmember as JSON
        render staffmember as JSON
    }
    
    protected void notFound() {
        request.withFormat {
            
            '*'{ render status: NOT_FOUND }
        }
    }
}

class StaffShift{
    
    public StaffShift(long id, String clientname, LocalDate shiftdate, String shifttype, int shifthours){
        this.id = id
        this.clientname = clientname
        this.shiftdate = shiftdate
        this.shifttype = shifttype
        this.shifthours = shifthours
    }
    
    long id
    String clientname
    LocalDate shiftdate
    String shifttype
    int shifthours
}

class StaffMessage{
    public StaffMessage (long id, String title, String message, String sender, Date datesent){
        this.id = id
        this.title = title
        this.message = message
        this.sender = sender
        this.datesent = datesent
    }
    
    long id
    String title
    String message
    String sender
    Date datesent
}
