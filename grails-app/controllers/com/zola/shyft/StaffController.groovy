package com.zola.shyft

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.context.ServletContextHolder

@Transactional
class StaffController {

    def springSecurityService
    def fileUploadrService
    def persistenceService
    def pdfRenderingService
    
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Staff.list(params), model:[staffInstanceCount: Staff.count()]
    }

    def show(Staff staffInstance) {
        
        respond staffInstance
    }

    def create() {
        respond new Staff(params)
    }
    
    def uploadprofilepicture(){
        def staffInstance = Staff.get(params.id)
        print "${staffInstance.id}"
        def avatarImage = request.getFile('avatar')
        if(!avatarImage.isEmpty()){
            staffInstance.avatar = fileUploadrService.uploadFile(avatarImage,"${staffInstance.id}.png","avatarImage")
            staffInstance.save(flush:true)
        }
        redirect staffInstance
    }
    

    @Transactional
    def save(Staff staffInstance) {
        if (staffInstance == null) {
            notFound()
            return
        }

        if (staffInstance.hasErrors()) {
            respond staffInstance.errors, view:'create'
            return
        }
        staffInstance.save flush:true
          try{
        def staffpaypackage = new PayPackage(
            sleeprateWD : Double.parseDouble(params.sleeprateWD),
            sleeprateWE : Double.parseDouble(params.sleeprateWE),
            sleeprateBH : Double.parseDouble(params.sleeprateBH),
            earlyrateWE : Double.parseDouble(params.earlyrateWE),
            earlyrateWD : Double.parseDouble(params.earlyrateWD),
            earlyrateBH : Double.parseDouble(params.earlyrateBH),
            laterateWE : Double.parseDouble(params.laterateWE),
            laterateWD : Double.parseDouble(params.laterateWD),
            laterateBH : Double.parseDouble(params.laterateBH),
            wakenightrateWE : Double.parseDouble(params.wakenightrateWE),
            wakenightrateWD : Double.parseDouble(params.wakenightrateWD),
            wakenightrateBH : Double.parseDouble(params.wakenightrateBH),
            daycarerateWE : Double.parseDouble(params.daycarerateWE),
            daycarerateWD : Double.parseDouble(params.daycarerateWD),
            daycarerateBH : Double.parseDouble(params.daycarerateBH),
            longdayrateWD : Double.parseDouble(params.longdayrateWD),
            longdayrateWE : Double.parseDouble(params.longdayrateWE),
            longdayrateBH : Double.parseDouble(params.longdayrateBH), 
            staff:staffInstance
        ).save(flush:true)        
          }catch(NumberFormatException e){
              
          }
        def staffRole = Role.findByAuthority('ROLE_STAFF')

        println "staff paypackage saved"
        if(staffInstance.authorities.contains(staffRole)){
            UserRole.create staffInstance, staffRole
            println "Staff assigned to staff role"
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'staff.label', default: 'Staff'), staffInstance.id])
                redirect staffInstance
            }
            '*' { respond staffInstance, [status: CREATED] }
        }
    }

    def edit(Staff staffInstance) {
        respond staffInstance
    }
    
    def exportStaffToPDF(){
        println "Exporting"
        def staffInstance = Staff.get(params.id)
        def trainings = staffInstance.trainings.asList()
        def servletContext = ServletContextHolder.servletContext
        def storagePath = servletContext.getRealPath("${staffInstance.avatar}")
        def avatar = new File("${staffInstance.avatar}")
        def args = [template:"/pdftemplates/staffprofile", model:[staffInstance:staffInstance, avatar:avatar.bytes,trainings:trainings]]
        println "rendering pdf"
        pdfRenderingService.render(args+[controller:this], response)
    }
    
    def freestaff(){
        def freestaff = persistenceService.getFreeStaff()
        if(freestaff.isEmpty()){
            flash.message = "No Free Staff Found"
            redirect action:'index'
        }
        else{
            def pageTitle = "Free Staff"
            render view:'staff', model:[staffInstanceList:freestaff,pageTitle:pageTitle]
        }
    }
    
    def paststaff(){
        def curclient = Client.get(params.id)
        def previousshifts = Shift.findAll{
            home == curclient
        }
        if(previousshifts.size()>0){
            def mypaststaff = new ArrayList<Staff>()
            for(int i;i<previousshifts.size();i++){
                if(previousshifts[i].staffassigned != null)
                mypaststaff.add(previousshifts[i].staffassigned)
            }
            if(mypaststaff.isEmpty()){
                flash.message = "No Past Staff Found"
            redirect curclient
            }
            def pageTitle = "Past Staff to work at ${curclient}"
            render view:'staff', model:[staffInstanceList:mypaststaff,pageTitle:pageTitle]
        }
        else{
            flash.message = "No Past Staff Found"
            redirect curclient
        }
    }

    @Transactional
    def update(Staff staffInstance) {
        if (staffInstance == null) {
            notFound()
            return
        }

        if (staffInstance.hasErrors()) {
            respond staffInstance.errors, view:'edit'
            return
        }

        staffInstance.save flush:true
        
         try{
        def staffpaypackage = new PayPackage(
            sleeprateWD : Double.parseDouble(params.sleeprateWD),
            sleeprateWE : Double.parseDouble(params.sleeprateWE),
            sleeprateBH : Double.parseDouble(params.sleeprateBH),
            earlyrateWE : Double.parseDouble(params.earlyrateWE),
            earlyrateWD : Double.parseDouble(params.earlyrateWD),
            earlyrateBH : Double.parseDouble(params.earlyrateBH),
            laterateWE : Double.parseDouble(params.laterateWE),
            laterateWD : Double.parseDouble(params.laterateWD),
            laterateBH : Double.parseDouble(params.laterateBH),
            wakenightrateWE : Double.parseDouble(params.wakenightrateWE),
            wakenightrateWD : Double.parseDouble(params.wakenightrateWD),
            wakenightrateBH : Double.parseDouble(params.wakenightrateBH),
            daycarerateWE : Double.parseDouble(params.daycarerateWE),
            daycarerateWD : Double.parseDouble(params.daycarerateWD),
            daycarerateBH : Double.parseDouble(params.daycarerateBH),
            longdayrateWD : Double.parseDouble(params.longdayrateWD),
            longdayrateWE : Double.parseDouble(params.longdayrateWE),
            longdayrateBH : Double.parseDouble(params.longdayrateBH), 
            staff:staffInstance
        ).save(flush:true)        
          }catch(NumberFormatException e){
              
          }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Staff.label', default: 'Staff'), staffInstance.id])
                redirect staffInstance
            }
            '*'{ respond staffInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Staff staffInstance) {

        if (staffInstance == null) {
            notFound()
            return
        }

        staffInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Staff.label', default: 'Staff'), staffInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'staff.label', default: 'Staff'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
    
    
}
