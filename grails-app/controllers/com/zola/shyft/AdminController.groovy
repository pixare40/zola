package com.zola.shyft

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

@Secured("ROLE_ADMIN")
class AdminController {

    def persistenceService
    def springSecurityService
    def shiftSearchService
    def messagingService
    
    static navigationScope="admin"
    
    def index() {
        def shifts = persistenceService.getPastShiftsByMonth()
        def shiftsstring = "${shifts}"
        def nursenumber = persistenceService.getNumberofNurses()
        def supportworkersnumber = persistenceService.getNumberofSupportWorkers()
        def carersnumber = persistenceService.getNumberofCarers()
        def clientnumber = persistenceService.getNumberOfClient()
        def shiftstoday = persistenceService.getNumberOfShiftsToday()
        def shifthourstoday = persistenceService.getNumberOfShiftHoursToday()
        def shiftsthisweek = persistenceService.getNumberOfShiftsThisWeek()
        def shifthoursthisweek = persistenceService.getNumberOfShiftHoursThisWeek()
        def shiftsthismonth = persistenceService.getNumberOfShiftsThisMonth()
        def shifthoursthismonth = persistenceService.getNumberOfShiftHoursThisMonth()
        
        
        render view:'index', model:[shifts:shifts,shiftsstring:shiftsstring, nursenumber:nursenumber, 
            supportnumber:supportworkersnumber, carersnumber:carersnumber, clientnumber:clientnumber,
            shiftstoday:shiftstoday,shiftsthisweek:shiftsthisweek, shiftsthismonth:shiftsthismonth, shifthourstoday:shifthourstoday,
            shifthoursthisweek:shifthoursthisweek,shifthoursthismonth:shifthoursthismonth]
    }
    
    def newshift(){
        def stafflist = persistenceService.getClients()
        
        render view:'/admin/shiftpages/newshift', model:[clientlist:stafflist]
    }
    
    def messaging(){
        def curuser = lookupuser();
        def sentmessages = messagingService.getAdminSentMessages(curuser)
        //def inbox = messagingService.getAdminReceivedMessages(curuser)
        render view:'/message/index', model:[messageInstanceList:sentmessages]
    }
    
    private lookupuser(){
        User.get(springSecurityService.principal.id)
    }
    
    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: ['The Record', params.id])
                redirect action: "index", method: "GET"
            }
        }
    }
}
