package com.zola.shyft



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ShiftController {

    def persistenceService
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Shift.list(params), model:[shiftInstanceCount: Shift.count()]
    }

    def show(Shift shiftInstance) {
        respond shiftInstance
    }

    def create() {
        respond new Shift(params)
    }

    @Transactional
    def save(Shift shiftInstance) {
        if (shiftInstance == null) {
            notFound()
            return
        }

        if (shiftInstance.hasErrors()) {
            respond shiftInstance.errors, view:'create'
            return
        }

        shiftInstance.save flush:true
        
        if(shiftInstance.shifttype == "Early"){
            shiftInstance.shifthours = 6
            shiftInstance.save flush:true
        }
        else if(shiftInstance.shifttype == "Longday"){
            shiftInstance.shifthours = 10
            shiftInstance.save flush:true
        }
        else if(shiftInstance.shifttype == "Late"){
            shiftInstance.shifthours = 7
            shiftInstance.save flush:true
        }
        else if(shiftInstance.shifttype == "Sleep"){
            shiftInstance.shifthours = 1
            shiftInstance.save flush:true
        }
        
        if(shiftInstance.staffassigned != null){
        sendNotification(shiftInstance)
        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'shift.label', default: 'Shift'), shiftInstance.id])
                redirect shiftInstance
            }
            '*' { respond shiftInstance, [status: CREATED] }
        }
    }

    private sendNotification(Shift shiftInstance){
        sendMail {     
            to "${shiftInstance.staffassigned.email}"     
            subject "New Shift"     
            body (view:'/mailtemplates/shiftnotification',model:[shiftInstance:shiftInstance])
          }
    }
    
    def checkShiftNotification(){
        def shiftInstance = Shift.get(1)
        render view:'/mailtemplates/shiftnotification',model:[shiftInstance:shiftInstance]
    }
    
    def unassigned(){
        def unassignedshifts = persistenceService.getUnassignedShifts()
        def shiftscount = unassignedshifts.size()
        def pageTitle = "Unassigned Shifts"
        render view:'shifts', model:[pageTitle:pageTitle,shiftInstanceList:unassignedshifts,shiftInstanceCount:shiftscount]
    }
    
    def pastshifts(){
        def curclient = Client.get(params.id)
        def pastshifts = Shift.findAll{
            home == curclient
        }
        def pageTitle = "Past Shifts at ${curclient}"
        render view:'shifts', model:[pageTitle:pageTitle,shiftInstanceList:pastshifts]
    }
    
    def edit(Shift shiftInstance) {
        respond shiftInstance
    }

    @Transactional
    def update(Shift shiftInstance) {
        if (shiftInstance == null) {
            notFound()
            return
        }

        if (shiftInstance.hasErrors()) {
            respond shiftInstance.errors, view:'edit'
            return
        }

        shiftInstance.save flush:true

        if(shiftInstance.shifttype == "Early"){
            shiftInstance.shifthours = 6
            shiftInstance.save flush:true
        }
        else if(shiftInstance.shifttype == "Longday"){
            shiftInstance.shifthours = 10
            shiftInstance.save flush:true
        }
        else if(shiftInstance.shifttype == "Late"){
            shiftInstance.shifthours = 7
            shiftInstance.save flush:true
        }
        else if(shiftInstance.shifttype == "Sleep"){
            shiftInstance.shifthours = 1
            shiftInstance.save flush:true
        }
        
        if(shiftInstance.staffassigned != null){
        sendNotification(shiftInstance)
        }
        
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Shift.label', default: 'Shift'), shiftInstance.id])
                redirect shiftInstance
            }
            '*'{ respond shiftInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Shift shiftInstance) {

        if (shiftInstance == null) {
            notFound()
            return
        }

        shiftInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Shift.label', default: 'Shift'), shiftInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'shift.label', default: 'Shift'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
