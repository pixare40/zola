package com.zola.shyft

import com.zola.shyft.*
import org.joda.time.DateTime
import org.joda.time.*
import org.jadira.usertype.dateandtime.joda.*

class ShiftsAggregatorJob {
    static triggers = {
        cron name:'Trigger', cronExpression:"0 59 22 * * ?"
    }

    def execute() {
        // execute job
        def currentdate = new LocalDate()
        def currentdayofthemonth = currentdate.getDayOfMonth()
        def currentmonth = currentdate.getMonthOfYear()
        def currentyear = currentdate.getYear()
        def c = Shift.createCriteria()
        def results = c.list{
            between('shiftdate', new LocalDate().minusDays(currentdayofthemonth),new LocalDate())
        }
        def resultstotal = results.size()
        def yearlyshiftdata = YearShiftData.findByYear(currentyear)?:new YearShiftData(year:currentyear).save(failOnError:true)
        
        if(currentmonth == 1){
            yearlyshiftdata.january = resultstotal
            yearlyshiftdata.save(flush:true)
        }
        else if (currentmonth == 2){
            yearlyshiftdata.february = resultstotal
            yearlyshiftdata.save(flush:true)
        }
        else if (currentmonth == 3){
            yearlyshiftdata.march = resultstotal
            yearlyshiftdata.save(flush:true)
        }
        else if (currentmonth == 4){
            yearlyshiftdata.april = resultstotal
            yearlyshiftdata.save(flush:true)
        }
        else if (currentmonth == 5){
            yearlyshiftdata.may = resultstotal
            yearlyshiftdata.save(flush:true)
        }
        else if (currentmonth == 6){
            yearlyshiftdata.june = resultstotal
            yearlyshiftdata.save(flush:true)
        }
        else if (currentmonth == 7){
            yearlyshiftdata.july = resultstotal
            yearlyshiftdata.save(flush:true)
        }
        else if (currentmonth == 8){
            yearlyshiftdata.august = resultstotal
            yearlyshiftdata.save(flush:true)
        }
        else if (currentmonth == 9){
            yearlyshiftdata.september = resultstotal
            yearlyshiftdata.save(flush:true)
        }
        else if (currentmonth == 10){
            yearlyshiftdata.october = resultstotal
            yearlyshiftdata.save(flush:true)
        }
        else if (currentmonth == 11){
            yearlyshiftdata.november = resultstotal
            yearlyshiftdata.save(flush:true)
        }
        else if (currentmonth == 12){
            yearlyshiftdata.december = resultstotal
            yearlyshiftdata.save(flush:true)
        }
        
        print "Job run succesfully"
        
    }
}
